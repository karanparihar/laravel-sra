<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sras', function (Blueprint $table) {
            $table->id();
            $table->integer('is_previous');
            $table->integer('id_previous');
            $table->integer('group');
            $table->string('provider');
            $table->integer('location');
            $table->string('year');
            $table->integer('created_by');
            $table->integer('updated_by');           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sras');
    }
}
