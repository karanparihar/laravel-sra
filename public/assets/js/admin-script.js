var _token = $('meta[name="csrf-token"]').attr('content');
 
 $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })

$(function () {
  $("#example1").DataTable({
    "responsive": true,
    "autoWidth": true,
  });
  $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "responsive": true,
  });

  $('.textarea').summernote({
    height: 500,
    focus: true
  });

  $("input[data-bootstrap-switch]").each(function () {
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
  $('.select2').select2()
});


window.addEventListener("DOMContentLoaded", function () {
  var sraCount = $("#section-sorting").data('count');
  for(i = 0; i < 3; i++) {
      slist("sortlist-"+i, i);
  }
});

window.addEventListener("DOMContentLoaded", function () {
  sortFormField("sortFormField");
});


function slist(target, sraIndex) {
  // (A) GET LIST + ATTACH CSS CLASS
  target = document.getElementById(target);
  target.classList.add("slist");

  // (B) MAKE ITEMS DRAGGABLE + SORTABLE
  var items = target.getElementsByTagName("li"),
    current = null;
  for (let i of items) {
    // (B1) ATTACH DRAGGABLE
    i.draggable = true;

    // (B2) DRAG START - YELLOW HIGHLIGHT DROPZONES
    i.addEventListener("dragstart", function (ev) {
      current = this;
      for (let it of items) {
        if (it != current) {
          it.classList.add("hint");
        }
      }
    });

    // (B3) DRAG ENTER - RED HIGHLIGHT DROPZONE
    i.addEventListener("dragenter", function (ev) {
      if (this != current) {
        this.classList.add("active");
      }
    });

    // (B4) DRAG LEAVE - REMOVE RED HIGHLIGHT
    i.addEventListener("dragleave", function () {
      this.classList.remove("active");
    });

    // (B5) DRAG END - REMOVE ALL HIGHLIGHTS
    i.addEventListener("dragend", function () {
      for (let it of items) {
        it.classList.remove("hint");
        it.classList.remove("active");
      }
    });

    // (B6) DRAG OVER - PREVENT THE DEFAULT "DROP", SO WE CAN DO OUR OWN
    i.addEventListener("dragover", function (evt) {
      evt.preventDefault();
    });

    // (B7) ON DROP - DO SOMETHING
    i.addEventListener("drop", function (evt) {
      evt.preventDefault();
      if (this != current) {
        let currentpos = 0,
          droppedpos = 0;
        for (let it = 0; it < items.length; it++) {
         if (current == items[it]) {
               currentpos = it;
          }
          if (this == items[it]) {
               droppedpos = it;
          }
          
        }
        if (currentpos < droppedpos) {
             this.parentNode.insertBefore(current, this.nextSibling);
        } else {
             this.parentNode.insertBefore(current, this);
        }
        var sectionOrdering = [];

        $("#sortlist-"+sraIndex+" li").each(function () {
             sectionOrdering.push({
              'section_id' : $(this).data('id'),
              'order'      : $(this).data('order')
            });
        });
         
        $.ajax({
            
            type: "post",
            url: '/admin/section-ordering',
            data: {
               "_token" : $('meta[name="csrf-token"]').attr('content'),
               'order' :  sectionOrdering
            },
            beforeSend: function(){
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top-center',
                    showConfirmButton: false,
                    timer: 10000
                }); 
                Toast.fire({
                    icon: 'warning',
                    title: 'Please wait. Processing your rquest'
                });
            },

            success: function(response) {
                 //console.log('response', response);
                 window.location.reload();
            }, 
            complete:function(data){
                 var Toast = Swal.mixin({
                    toast: true,
                    position: 'top-center',
                    showConfirmButton: true,
                    timer: 3000
                }); 
                Toast.fire({
                    icon: 'success',
                    title: 'Done'
                });

            },
            error: function(error) {
                  console.log('response', response);
            }

        });


      }
    });
  }
}

function sortFormField(target) {
  // (A) GET LIST + ATTACH CSS CLASS
  target = document.getElementById(target);
  target.classList.add("slist");

  // (B) MAKE ITEMS DRAGGABLE + SORTABLE
  var items = target.getElementsByTagName("li"),
    current = null;
  for (let i of items) {
    // (B1) ATTACH DRAGGABLE
    i.draggable = true;

    // (B2) DRAG START - YELLOW HIGHLIGHT DROPZONES
    i.addEventListener("dragstart", function (ev) {
      current = this;
      for (let it of items) {
        if (it != current) {
          it.classList.add("hint");
        }
      }
    });

    // (B3) DRAG ENTER - RED HIGHLIGHT DROPZONE
    i.addEventListener("dragenter", function (ev) {
      if (this != current) {
        this.classList.add("active");
      }
    });

    // (B4) DRAG LEAVE - REMOVE RED HIGHLIGHT
    i.addEventListener("dragleave", function () {
      this.classList.remove("active");
    });

    // (B5) DRAG END - REMOVE ALL HIGHLIGHTS
    i.addEventListener("dragend", function () {
      for (let it of items) {
        it.classList.remove("hint");
        it.classList.remove("active");
      }
    });

    // (B6) DRAG OVER - PREVENT THE DEFAULT "DROP", SO WE CAN DO OUR OWN
    i.addEventListener("dragover", function (evt) {
      evt.preventDefault();
    });

    // (B7) ON DROP - DO SOMETHING
    i.addEventListener("drop", function (evt) {
      evt.preventDefault();
      if (this != current) {
        let currentpos = 0,
          droppedpos = 0;
        for (let it = 0; it < items.length; it++) {
         if (current == items[it]) {
               currentpos = it;
          }
          if (this == items[it]) {
               droppedpos = it;
          }
          
        }
        if (currentpos < droppedpos) {
             this.parentNode.insertBefore(current, this.nextSibling);
        } else {
             this.parentNode.insertBefore(current, this);
        }
        var formFieldOrdering = [];
        $("#sortFormField li").each(function () {
             formFieldOrdering.push({
              'field_id' : $(this).data('id'),
              'order'      : $(this).data('order')
            });
        });

        console.log('formFieldOrdering' + formFieldOrdering);


        $.ajax({
            
            type: "post",
            url: '/admin/form-fields',
            data: {
               "_token" : _token,
               'order' :  formFieldOrdering
            },
            beforeSend: function(){
                var Toast = Swal.mixin({
                    toast: true,
                    position: 'top-center',
                    showConfirmButton: false,
                    timer: 10000
                }); 
                Toast.fire({
                    icon: 'warning',
                    title: 'Please wait. Processing your rquest'
                });
            },

            success: function(response) {
                 //console.log('response', response);
                 window.location.reload();
            }, 
            complete:function(data){
                 var Toast = Swal.mixin({
                    toast: true,
                    position: 'top-center',
                    showConfirmButton: true,
                    timer: 3000
                }); 
                Toast.fire({
                    icon: 'success',
                    title: 'Done'
                });

            },
            error: function(error) {
                  console.log('response', response);
            }

        });


      }
    });
  }
}



$(document).ready( function() {
    
    $("#select-builder").change(function() {
          var value = $(this).val();
         
          if(value == 1) {

            $("#create-table-layout").show();
            $("#create-form-layout").hide();

          } else if(value == 2) {

             $("#create-table-layout").hide();
             $("#create-form-layout").show();

          } else {
             $("#create-table-layout").hide();
             $("#create-form-layout").hide();
          }

    });

    $("#create-table").click(function(){
          var number_of_rows = $('#row-val').val();
          var number_of_cols = $('#col-val').val();
          
          $.ajax({
              type: "post",
              url: '/admin/create-table',
              data: {
                 "_token" : $('meta[name="csrf-token"]').attr('content'),
                 'rows' :  number_of_rows,
                 'cols' :  number_of_cols
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    
                  $('#table-layout').show();
                  $('#table-layout .table-layout').html(response.message);
                   
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          });  
    });
    

    $("#field-type").change(function() {
       
         var value = $(this).val(); 
        
         if(value == 3  || value == 4) {
            $("#if-checkbox-radio").show();
         } else {
            $("#if-checkbox-radio").hide();
         }
    });


    $("#create-form-field").click(function(){
          var type     = $('#field-type').val();
          var loop     = $('#loop').val();
          var required = $('#required').val();
          
          $.ajax({
              type: "post",
              url: '/admin/create-form-field',
              data: {
                 "_token"  : $('meta[name="csrf-token"]').attr('content'),
                 'type'    :  type,
                 'loop'    :  loop,
                 'required': required
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    
                  $('#form-layout').show();
                  $('#form-layout .form-layout').html(response.message);
                   
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          });  
    });
  
});

function addForm(attribute) {
     
     if(attribute) {

         $.ajax({
              type: "post",
              url: '/admin/create-form-field',
              data: {
                 "_token"   : _token,
                 "attribute": attribute
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    
                  $('#modal-lg').modal('show');
                  $('#modal-lg .modal-title').html(response.state);
                  $('#modal-lg .modal-body').html(response.message);
                   
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          }); 
     }
}

 $('#generate-field').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: '/admin/manage-form',
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function(){
              $("#save-field").attr('disabled', true);
            },
            success: function(response) {

                $('#generate-field').trigger("reset");
                $('#modal-lg').modal('hide');

                var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: response.status
                  });
                  window.location.reload();
            },
            complete:function(data) {
                 
                 $("#save-field").attr('disabled', false);
                 
               
            }, error:function(error) {
                
                var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'error',
                      title: error.responseJSON.errors.name
                  });
            }
        });
 });

 function viewFormField(id) {

      $.ajax({
              type: "post",
              url: '/admin/show-form-field',
              data: {
                 "_token"   : _token,
                 "attribute": id
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    
                  $('#modal-view-form-field').modal('show');
                  $('#modal-view-form-field .modal-body').html(response.message);
                  $('#modal-view-form-field .modal-title').html(response.state);
                  
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          }); 
 }

 function commonDeleteAjax(url) {
    
    const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-success'
            },
            buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "Record will be deleted permanently.",
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel',
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    data: {"_token": _token },
                    success: function(response) {

                      if (response.status) {
                          Swal.fire({
                                  icon: 'success',
                                  title: 'Deleted!',
                                  text: response.status, 
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                              });
                           } else {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Error',
                                  text:  response.status,  
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                           });
                        }
                    },
                });
            } else if (

           result.dismiss === Swal.DismissReason.cancel
            ) {
               
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your record is safe.',
                    'info'
                )

            }
        })
}

function activateDeactivate(status, id) {

      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-success'
            },
            buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "To change this field status from the sections?",
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Change',
            cancelButtonText: 'Cancel',
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/change-field-status',
                    data: {"_token": _token, 'status': status, 'id': id },
                    success: function(response) {

                      if (response.status) {
                          Swal.fire({
                                  icon: 'success',
                                  title: 'Status changed',
                                  text: response.status, 
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                              });
                           } else {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Error',
                                  text:  response.status,  
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                           });
                        }
                    },
                });
            } else if (

           result.dismiss === Swal.DismissReason.cancel
            ) {
               
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your record is safe.',
                    'info'
                )

            }
        })
}

function sectionStatus(status, id) {

      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-success'
            },
            buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "To change this field status from the sections?",
            icon: 'error',
            showCancelButton: true,
            confirmButtonText: 'Change',
            cancelButtonText: 'Cancel',
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/change-section-status',
                    data: {"_token": _token, 'status': status, 'id': id },
                    success: function(response) {

                      if (response.status) {
                          Swal.fire({
                                  icon: 'success',
                                  title: 'Status changed',
                                  text: response.status, 
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                              });
                           } else {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Error',
                                  text:  response.status,  
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                           });
                        }
                    },
                });
            } else if (

           result.dismiss === Swal.DismissReason.cancel
            ) {
               
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your record is safe.',
                    'info'
                )

            }
        })
}

$("#groups").change(function() {
   var id = $(this).val();
   $.ajax({
              type: "get",
              url: '/admin/sra/create',
              data: {
                 "_token"   : _token,
                 "group"    : id
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    $("#provider-field").show();
                    $("#provider-field").html(response.message);  
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          }); 
});

$("#get-sections").change(function() {
   var id = $(this).val();
 
   $.ajax({
              type: "get",
              url: '/admin/users/create',
              data: {
                 "_token"   : _token,
                 "sra"    : id
              },
              beforeSend: function(){
                  var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: false,
                      timer: 10000
                  }); 
                  Toast.fire({
                      icon: 'warning',
                      title: 'Please wait. Processing your rquest'
                  });
              },

              success: function(response) {
                    $("#user-sections").show();
                    $("#user-sections").html(response.message);  
              }, 
              complete:function(data){
                   var Toast = Swal.mixin({
                      toast: true,
                      position: 'top-center',
                      showConfirmButton: true,
                      timer: 3000
                  }); 
                  Toast.fire({
                      icon: 'success',
                      title: 'Done'
                  });
              },
              error: function(error) {
                    console.log('response', response);
              }
          }); 
});

function sraStatus(status, id) {

      const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-success'
            },
            buttonsStyling: true
        })
        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "To mark this SRA as completed",
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: 'Change',
            cancelButtonText: 'Cancel',
            reverseButtons: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: '/admin/change-sra-status',
                    data: {"_token": _token, 'status': status, 'id': id },
                    success: function(response) {

                      if (response.status) {
                          Swal.fire({
                                  icon: 'success',
                                  title: 'Status changed',
                                  text: response.status, 
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                              });
                           } else {
                              Swal.fire({
                                  icon: 'error',
                                  title: 'Error',
                                  text:  response.status,  
                              }).then((result) => {
                                if(result.value) {
                                  window.location.reload();
                                }                                  
                           });
                        }
                    },
                });
            } else if (

           result.dismiss === Swal.DismissReason.cancel
            ) {
               
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your record is safe.',
                    'info'
                )

            }
        })
}