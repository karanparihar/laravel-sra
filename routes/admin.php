<?php

/*
|--------------------------------------------------------------------------
| Web Routes :::: Admin
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth', 'verified']], function() {
 
	Route::get('/admin/dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('admin.dashboard');

	Route::resource('admin/sections', App\Http\Controllers\Admin\SectionsController::class, ['except' => ['']]);

	Route::resource('admin/section-ordering', App\Http\Controllers\Admin\SectionSortingController::class, ['except' => []]);

	Route::resource('admin/manage-form', App\Http\Controllers\Admin\FormBuilderController::class, ['except' => ['create']]);

	Route::resource('admin/users', App\Http\Controllers\Admin\UsersController::class, ['except' => ['']]);
   Route::resource('admin/change-password', App\Http\Controllers\Admin\ChangePasswordController::class, ['except' => ['show', 'edit', 'destroy','update', 'create']]);
   
   Route::resource('admin/profile-setting', App\Http\Controllers\Admin\ProfileSettingController::class, ['except' => ['show', 'edit', 'destroy','update', 'create']]);

   Route::resource('admin/developer-console', App\Http\Controllers\Admin\ArtisanConsoleController::class, ['except' => []]);

   Route::resource('admin/section-type', App\Http\Controllers\Admin\SraTypeController::class, ['except' => ['show', 'delete']]);

   Route::resource('admin/sra', App\Http\Controllers\Admin\SraController::class, ['except' => []]);
   
   Route::post('admin/change-field-status', [App\Http\Controllers\Admin\AjaxHandlerController::class, 'index']);

   Route::post('admin/change-section-status', [App\Http\Controllers\Admin\AjaxHandlerController::class, 'changeSecionStatus']);

    Route::post('admin/change-sra-status', [App\Http\Controllers\Admin\AjaxHandlerController::class, 'changeSraStatus']);
   
   Route::post('admin/create-form-field', [App\Http\Controllers\Admin\AjaxHandlerController::class, 'createFormField']);

   Route::post('admin/show-form-field', [App\Http\Controllers\Admin\AjaxHandlerController::class, 'showFormField']);

   Route::resource('admin/form-fields', App\Http\Controllers\Admin\FormFieldOrderingController::class, ['except' => ['create', 'index', 'edit', 'update', 'show', 'destroy']]);

   Route::resource('admin/media', App\Http\Controllers\Admin\MediaController::class, ['except' => ['edit', 'update']]);

   Route::resource('admin/location', App\Http\Controllers\Admin\LocationController::class, ['except' => []]);

   Route::resource('admin/group', App\Http\Controllers\Admin\GroupController::class, ['except' => []]);

   Route::resource('admin/provider', App\Http\Controllers\Admin\ProviderController::class, ['except' => []]);

   Route::resource('admin/sra-map', App\Http\Controllers\Admin\SraMappingController::class, ['except' => []]);

   Route::resource('admin/sra-document', App\Http\Controllers\Admin\SraDocumentController::class, ['except' => []]);

   Route::resource('admin/sra-report', App\Http\Controllers\Admin\SraReportController::class, ['except' => []]);


   Route::get('admin/completed-sra', [App\Http\Controllers\Admin\CommonController::class, 'index'])->name('completed.sra');


});

