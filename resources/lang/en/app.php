<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to display application language.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
     'model' => [
         'section'          => 'Section',
         'user'             => 'User',
         'change-password'  => 'Change Password',
         'artisan'          => 'Artisan Command',
         'form_field'       => 'Form Field',
         'sra'              => 'SRA',
         'sratype'          => 'SRA type',
         'media'            => 'Media',
         'location'         => 'Location',
         'group'            => 'Group',
         'provider'         => 'Provider',
         'sra_map'          => 'Sra Section',
         'sra_document'     => 'Sra Document'
         
         
     ],

     'buttons' => [
         'save'   => 'Save',
         'drart'  => 'Draft',
         'cancel' => 'Cancel'
     ],

     'actions' => [
        'edit'     => 'Edit',
        'trash'    => 'Delete',
        'view'     => 'View',
        'run'      => 'Run',
        'active'   =>  'Active',
        'inactive' => 'In-Active',
        'proceed'  => 'Manage SRA',
        'pdf'      => 'View SRA',
        

     ],

     'action_class' => [
        'edit'   => 'warning',
        'view'   => 'success',
        'delete' => 'danger'
     ],

     'dropdown' => [
        'active'   => 'Active',
        'inactive' => 'In-Active',
        'table'    => 'Table',
        'form'     => 'Form',
        'option'   => 'Select an option'
     ]



];