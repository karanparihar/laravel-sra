@extends('layouts.login')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                
                <form method="POST" action="{{ route('password.update') }}" class="box">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">

                    <h1>{{ config('app.name', 'Laravel') }} <br> <small>
                     {{ __('Reset Password') }} </small>
                    </h1>

                    <input type="email" name="email" placeholder="Email" class="@error('email') is-invalid @enderror"  value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus> 
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <input type="password" name="password" placeholder="Password" class="@error('password') is-invalid @enderror" required autocomplete="new-password"> 

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror


                     <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">

                    

                    <input type="submit" name="submit" value="{{ __('Reset Password') }}">
                   
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
