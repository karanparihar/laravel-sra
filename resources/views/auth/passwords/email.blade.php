@extends('layouts.login')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                
                <form action="{{ route('password.email') }}" method="POST" class="box">
                    @csrf
                    <h1>{{ config('app.name', 'Laravel') }} <br> <small>
                     {{ __('Reset Password') }} </small>
                    </h1>
                    <p class="text-muted">Please enter registered email address!</p>
                    @if (session('status'))
                        <p class="text-success">
                            {{ session('status') }}
                        </p>
                    @endif
                    <input type="email" name="email" placeholder="Email" class="@error('email') is-invalid @enderror" required autocomplete="email" autofocus> 
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    <a class="forgot text-muted" href="{{ route('login') }}">Login</a> 

                    <input type="submit" name="submit" value="{{ __('Send Password Reset Link') }}">
                   
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
