<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="index3.html" class="brand-link">
   <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
   <span class="brand-text font-weight-light">{{ config('app.name', 'Laravel') }}</span>
   </a>
   <!-- Sidebar -->
   <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
         <div class="image">
            <img src="{{ asset('assets/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
         </div>
         <div class="info">
            <a href="#" class="d-block">{{ Auth::user()->name }}</a>
         </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
               <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Route::currentRouteName() == 'admin.dashboard' ? 'active' : '' }}">
                  <i class="nav-icon fas fa-tachometer-alt text-orange"></i>
                  <p>
                     Dashboard
                     <i class="fas {{ Route::currentRouteName() == 'admin.dashboard' ? 'fa-angle-right' : 'fa-angle-left' }} right"></i>
                  </p>
               </a>
            </li>
            @if(Auth::user()->role == 1)
            <li class="nav-item has-treeview">
               <a href="{{ route('users.index') }}" class="nav-link {{ Route::currentRouteName() == 'users.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users.edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'users.show' ? 'active' : '' }}">
                  <i class="nav-icon fas fa-users text-pink"></i>
                  <p>
                     Users
                     <i class="fas {{ Route::currentRouteName() == 'users.index' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'users.create' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'users.edit' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'users.show' ? 'fa-angle-right' : 'fa-angle-left' }} right"></i>
                  </p>
               </a>
            </li>
            
            <li class="nav-item has-treeview">
               <a href="{{ route('manage-form.index') }}" class="nav-link {{ Route::currentRouteName() == 'manage-form.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'manage-form.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'manage-form.edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'manage-form.show' ? 'active' : '' }}">
                  <i class="nav-icon fas fa-building text-purple"></i>
                  <p>
                     Form Builder
                     <i class="fas {{ Route::currentRouteName() == 'manage-form.index' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'manage-form.create' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'manage-form.edit' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'manage-form.show' ? 'fa-angle-right' : 'fa-angle-left' }} right"></i>
                  </p>
               </a>
            </li>

            <li class="nav-item has-treeview {{ Route::currentRouteName() == 'sections.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'sections.show' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'section-ordering.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'section-type.index' ? 'menu-open' : '' }}">

               <a href="#" class="nav-link {{ Route::currentRouteName() == 'sections.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sections.show' ? 'active' : '' }} || {{ Route::currentRouteName() == 'section-ordering.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'section-type.index' ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-puzzle-piece text-warning"></i>
                  <p>
                     Sections
                     <i class="fas {{ Route::currentRouteName() == 'sections.index' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'sections.show' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'ssection-ordering.index' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'section-type.index' ? 'fa-angle-left' : 'fa-angle-right' }} right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{ route('sections.index') }}" class="nav-link {{ Route::currentRouteName() == 'sections.index'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'sections.show' ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Manage Section</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{ route('section-ordering.index') }}" class="nav-link {{ Route::currentRouteName() == 'section-ordering.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Order Section</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{ route('section-type.index') }}" class="nav-link {{ Route::currentRouteName() == 'section-type.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-teal"></i>
                        <p>Section Type</p>
                     </a>
                  </li>
                
               </ul>
            </li>

             @endif

            <li class="nav-item has-treeview {{ Route::currentRouteName() == 'sra.create' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'sra.index' ? 'menu-open' : '' }}  || {{ Route::currentRouteName() == 'sra.edit' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'sra-report.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'sra.show' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'sra-report.show' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'completed.sra' ? 'menu-open' : '' }} ">

               <a href="#" class="nav-link {{ Route::currentRouteName() == 'sra.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra.edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra-report.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra.show' ? 'active' : '' }}  }  || {{ Route::currentRouteName() == 'sra-report.show' ? 'active' : '' }} || {{ Route::currentRouteName() == 'completed.sra' ? 'active' : '' }} ">

                  <i class="nav-icon fas fa-step-forward text-teal"></i>
                  <p>
                     SRA
                     <i class="fas {{ Route::currentRouteName() == 'sra.create' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'sra.index' ? 'fa-angle-left' : 'fa-angle-right' }}  right"></i>
                  </p>
               </a>

               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{ route('sra.create') }}" class="nav-link {{ Route::currentRouteName() == 'sra.create'  ? 'active' : '' }} ">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Create SRA</p>
                     </a>
                  </li>

                  <li class="nav-item">
                     <a href="{{ route('sra.index') }}" class="nav-link {{ Route::currentRouteName() == 'sra.index'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra.edit'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra.show'  ? 'active' : '' }} ">
                        <i class="far fa-circle nav-icon text-success"></i>
                        <p>Manage SRA</p>
                     </a>
                  </li>

                 <li class="nav-item">
                     <a href="{{ route('sra-report.index') }}" class="nav-link {{ Route::currentRouteName() == 'sra-report.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Report</p>
                     </a>
                  </li>

                  <li class="nav-item">
                     <a href="{{ route('completed.sra') }}" class="nav-link {{ Route::currentRouteName() == 'completed.sra'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'sra-report.show'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'completed.sra'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-teal"></i>
                        <p>Completed SRA</p>
                     </a>
                  </li>
                  
                  <!--<li class="nav-item">
                     <a href="{{ route('sra.index') }}" class="nav-link {{ Route::currentRouteName() == 'sra.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Deleted SRA</p>
                     </a>
                  </li> -->
               </ul>
            </li>
           
            <li class="nav-item has-treeview {{ Route::currentRouteName() == 'change-password.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'profile-setting.index' ? 'menu-open' : '' }}">
               <a href="#" class="nav-link {{ Route::currentRouteName() == 'change-password.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'profile-setting.index' ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-cog text-yellow"></i>
                  <p>
                     Account settings
                     <i class="fas {{ Route::currentRouteName() == 'change-password.index' ? 'fa-angle-left' : 'fa-angle-right' }} right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
               <!--    <li class="nav-item">
                     <a href="{{ route('profile-setting.index') }}" class="nav-link {{ Route::currentRouteName() == 'profile-setting.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Profile</p>
                     </a>
                  </li> -->
                  <li class="nav-item">
                     <a href="{{ route('change-password.index') }}" class="nav-link {{ Route::currentRouteName() == 'change-password.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Change Password</p>
                     </a>
                  </li>
               </ul>
            </li>

            <li class="nav-item has-treeview {{ Route::currentRouteName() == 'media.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'media.create' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'media.show' ? 'menu-open' : '' }} "> 

               <a href="#" class="nav-link {{ Route::currentRouteName() == 'media.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'media.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'media.show' ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-file text-white"></i>
                  <p>
                     Documents
                     <i class="fas {{ Route::currentRouteName() == 'media.index' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'media.create' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'media.show' ? 'fa-angle-left' : 'fa-angle-right' }}  right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{ route('media.create') }}" class="nav-link {{ Route::currentRouteName() == 'media.create'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Add New</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{ route('media.index') }}" class="nav-link {{ Route::currentRouteName() == 'media.index'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'media.show'  ? 'active' : '' }} ">
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Library</p>
                     </a>
                  </li>

               </ul>
            </li>

            <li class="nav-item has-treeview {{ Route::currentRouteName() == 'group.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'provider.index' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'provider.create' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'provider.edit' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'group.create' ? 'menu-open' : '' }} || {{ Route::currentRouteName() == 'group.edit' ? 'menu-open' : '' }}  "> 

               <a href="#" class="nav-link {{ Route::currentRouteName() == 'group.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'group.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'group.edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'provider.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'provider.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'provider.edit' ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-file-video text-white"></i>
                  <p>
                     Group & Provider
                     <i class="fas {{ Route::currentRouteName() == 'group.index' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'provider.index' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'group.create' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'group.edit' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'provider.create' ? 'fa-angle-left' : 'fa-angle-right' }} || {{ Route::currentRouteName() == 'provider.edit' ? 'fa-angle-left' : 'fa-angle-right' }} right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{ route('group.index') }}" class="nav-link {{ Route::currentRouteName() == 'group.index'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'group.create'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'group.edit'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon text-danger"></i>
                        <p>Group</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{ route('provider.index') }}" class="nav-link {{ Route::currentRouteName() == 'provider.index'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'provider.create'  ? 'active' : '' }} || {{ Route::currentRouteName() == 'provider.edit'  ? 'active' : '' }} "> 
                        <i class="far fa-circle nav-icon text-warning"></i>
                        <p>Provider</p>
                     </a>
                  </li>

               </ul>
            </li>

            <li class="nav-item has-treeview">
               <a href="{{ route('location.index') }}" class="nav-link {{ Route::currentRouteName() == 'location.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'location.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'location.edit' ? 'active' : '' }} || {{ Route::currentRouteName() == 'location.show' ? 'active' : '' }}">
                  <i class="nav-icon fas fa-location-arrow text-white"></i>
                  <p>
                     Locations
                     <i class="fas {{ Route::currentRouteName() == 'location.index' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'location.create' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'location.edit' ? 'fa-angle-right' : 'fa-angle-left' }} || {{ Route::currentRouteName() == 'location.show' ? 'fa-angle-right' : 'fa-angle-left' }} right"></i>
                  </p>
               </a>
            </li>

           

            <li class="nav-item has-treeview">
               <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();"  class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                     Logout
                  </p>
               </a>
               <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
               </form>
            </li>
            @if(Auth::user()->role == 1)
            <li class="nav-header">
              DEVELOPER CONSOLE
            </li>
            <li class="nav-item has-treeview">
               <a href="{{ route('developer-console.index') }}" class="nav-link {{ Route::currentRouteName() == 'developer-console.index' ? 'active' : '' }} || {{ Route::currentRouteName() == 'developer-console.create' ? 'active' : '' }} || {{ Route::currentRouteName() == 'developer-console.edit' ? 'active' : '' }}">
                  <i class="nav-icon fas fa-danger"></i>
                  <p>
                     Artisan Console
                     <i class="fas {{ Route::currentRouteName() == 'developer-console.index' ? 'fa-angle-right' : 'fa-angle-left' }} right"></i>
                  </p>
               </a>
            </li>

            @endif

         </ul>
      </nav>
      <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
</aside>