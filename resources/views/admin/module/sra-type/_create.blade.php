@extends('admin.layouts.master')

@section('title', 'Add Sra Type')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>

        {!! Form::open(['route' => 'section-type.store', 'files' => true]) !!}
        <div class="card-body">

          <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'required']) !!}
            {!! Form::text('name', old('name'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('description', 'Description', ['class' => 'awesome']) !!} 
            {!! Form::textarea('description', old('description'), array_merge(['class' => 'form-control'], ['required' => true], ['rows' => 4] ) ) !!}
             @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('section-type.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection