@extends('admin.layouts.master')

@section('title', 'Edit Provider')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>

        {!! Form::open(['route' => ['provider.update', $provider->id], 'files' => true, 'method'=>'PUT']) !!}

        <div class="card-body">

          <div class="form-group">
              {!! Form::label('group', 'Group', ['class' => 'required']) !!}
              {!! Form::select('group', $groups, 
                    $provider->group, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...']) 
                !!}
              @error('group')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'required']) !!}
            {!! Form::text('name', $provider->name, array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('description', 'Description', ['class' => 'awesome']) !!} 
            {!! Form::textarea('description', $provider->description, array_merge(['class' => 'form-control'], ['required' => false], ['rows' => 4] ) ) !!}
             @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('provider.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection