@extends('admin.layouts.master')

@section('title', 'View Section')

@section('content')

<style type="text/css">
   .slist {
     list-style: none;
     padding: 0;
     margin: 0;
   }
   .slist li {
     margin: 5px;
     padding: 10px;
     border: 1px solid #333;
     background: #eaeaea;
   }
   /* (B) DRAG-AND-DROP HINT */
   .slist li.hint { background: #fea; }
   .slist li.active { background: #ffd4d4; }
</style>
<section class="content">
   <div class="container-fluid">

      <div class="row">
         <div class="card-body">
            
             <div class="callout callout-info">
                  <h5>Drag and Drop</h5>
                  <p>{{ trans('messages.ordering.form_fields') }}</p>
                   <p>{{ trans('messages.ordering.active_inactive') }}</p>
             </div>
       
            <div id="accordion">
               <div class="card card-success">
                  <div class="card-header">
                     <h4 class="card-title w-100">
                        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                        {{ $section->name }}
                        </a>
                     </h4>
                  </div>
                  <div id="collapseOne" class="collapse show" data-parent="#accordion">
                     <div class="card-body">
                        <div class="tab-content p-0">
                           <!-- Morris chart - Sales -->
                           <ul id="sortFormField">
                              @foreach($form_fields as $data)

                                <!------------ TEXT ------------->
                                @if($data->field_type =='text')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">
                                   @include('admin.module.form-builder.fields._input', $data)
                                </li>
                                
                                <!------------ EMAIL ------------->

                                @elseif($data->field_type =='email')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">
                                   @include('admin.module.form-builder.fields._input', $data)
                                </li>

                                <!------------ URL ------------->

                                @elseif($data->field_type == 'url')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">
                                  @include('admin.module.form-builder.fields._url', $data) 
                                </li>

                                <!------------ NUMBER ------------->

                                @elseif($data->field_type == 'number')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}"> 
                                   @include('admin.module.form-builder.fields._number', $data) 
                                </li>

                                <!------------ TEXTAREA ------------->

                                @elseif($data->field_type == 'textarea')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}"> 
                                    @include('admin.module.form-builder.fields._textarea', $data) 
                                </li>

                                <!------------ RADIO BUTTONS ------------->

                                @elseif($data->field_type == 'radio-buttons')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">
                                   @include('admin.module.form-builder.fields._radio', $data) 
                                </li>

                                <!------------ CHECKBOXES ------------->

                                @elseif($data->field_type == 'checkboxes')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}"> 
                                    @include('admin.module.form-builder.fields._checkbox', $data)
                                 </li>

                                 <!------------ DROP DOWN ------------->

                                @elseif($data->field_type == 'drop-down')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">    
                                    @include('admin.module.form-builder.fields._select', $data)
                                </li>

                                <!------------ File ------------->

                                @elseif($data->field_type == 'file')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">    
                                    @include('admin.module.form-builder.fields._file', $data)
                                </li>

                                <!------------ DATE ------------->

                                @elseif($data->field_type == 'date')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">    
                                    @include('admin.module.form-builder.fields._date', $data)
                                </li>

                                <!------------ TEL ------------->

                                @elseif($data->field_type == 'tel')

                                <li data-id="{{ $data->id }}" data-order="{{ $data->ordering }}">    
                                    @include('admin.module.form-builder.fields._tel', $data)
                                </li>

                                @else

                                @endif

                              @endforeach
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection