@extends('admin.layouts.master')

@section('title', 'Edit section')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>
       
        </div>

        {!! Form::open(['route' => ['sections.update', $section->id], 'files' => true, 'method'=>'PUT']) !!}

        <div class="card-body">

          <div class="form-group">
              {!! Form::label('type', 'Sra type', ['class' => 'required']) !!}
              {!! Form::select('sra', $SraTypeAttr, 
                    $section->sra, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...']) 
                !!}
              @error('sra')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'required']) !!}
            {!! Form::text('name', old('name', $section->name), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('description', 'Description', ['class' => 'awesome']) !!} 
            {!! Form::textarea('description', old('description', $section->description), array_merge(['class' => 'form-control'], ['required' => true], ['rows' => 4] ) ) !!}
             @error('description')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
              {!! Form::label('status', 'Status', ['class' => 'required']) !!}
              {!! Form::select('status', [
                     '1' => trans('app.dropdown.active'), 
                     '2' => trans('app.dropdown.inactive')
                    ], 
                    $section->status, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...']) 
                !!}
              @error('status')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('class_attribute', 'Class attribute', ['class' => 'required']) !!}
            {!! Form::text('class_attribute', $section->class_attribute, array_merge(['class' => 'form-control'] ) ) !!}
              @error('class_attribute')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('id_attribute', 'Id attribute', ['class' => 'required']) !!}
            {!! Form::text('id_attribute', $section->class_attribute, array_merge(['class' => 'form-control'] ) ) !!}
              @error('id_attribute')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
      {!! link_to_route('sections.index',  'Cancel', $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->
@endsection