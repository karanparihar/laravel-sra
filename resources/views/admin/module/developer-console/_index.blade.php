@extends('admin.layouts.master')

@section('title', 'Artisan Console')

@section('content')

<!-- Main content -->
<section class="content">
   <div class="container-fluid">
     
      <div class="card">
            
            <div class="card-header">
                  <a href="{{ route('developer-console.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Command</a>
            </div>
           
            <!-- /.card-header -->
            <div class="card-body">
               <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                        <th>S.No</th>
                        <th>Command</th>
                        <th>Descrption</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($commands as $key => $value)
                     @php  $counter = 0 @endphp
                     <tr>
                        <td>{{ ++$counter }}</td>
                        <td>{{ $value->command }}</td>
                        <td>{{ $value->description }}</td>
                        <td>
                           {!! link_to_route('developer-console.edit',  trans('app.actions.edit'), $parameters = [$value->id ],  ['class' => 'btn btn-secondary']) !!}

                           {!! link_to_route('developer-console.show',  trans('app.actions.run'), $parameters = [$value->id],  ['class' => 'btn btn-success']) !!}  
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
                  <tfoot>
                     <tr>
                        <th>S.No</th>
                        <th>Title</th>
                        <th>Subject</th>
                        <th>Action</th>
                     </tr>
                  </tfoot>
               </table>
            </div>
            <!-- /.card-body -->
         
      </div>
      <!-- /.card -->
   </div>
</section>
@endsection