
@if($data->required == "1") 
   @php $required = true @endphp
@else
   @php $required = false @endphp
@endif


@if($data->placeholder == "1") 
   @php $placeholder = $data->values @endphp
@else
   @php $placeholder = '' @endphp
@endif

@if($data->status == "1") 
   @php $activeInactive = 'Active'; $class = 'text-success' @endphp
@else
     @php $activeInactive = 'InActive'; $class = 'text-danger' @endphp
@endif

<div class="form-group">
   {!! Form::label($data->name, ucwords($data->name) ) !!}  

   @if(Route::currentRouteName() == 'sections.show' || Route::currentRouteName() == 'users.show' )
      <sup class="{{ $class }}">[{{ $activeInactive }}]</sup>
   @endif

      {!! Form::date(str_slug($data->name), !empty($sraMappedData) ? $sraMappedData->meta_value : $data->values , [
         'class' => 'form-control ' .$data->class_attribute, 
         'required' => $required,
         'id' => $data->id_attribute, 
      ])  !!}
</div>