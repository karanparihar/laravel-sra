@extends('admin.layouts.master')

@section('title', 'Create New SRA')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
  
        {!! Form::open(['route' => 'sra.store', 'files' => true]) !!}
        
        <div class="card-body">
         
         <div class="form-group"> 

           {!! Form::label('previous sra', 'Generate from previous year', ['class' => 'required']) !!} 

           {!! Form::radio('previous_year', '1', '') !!}
           {!! Form::label('yes', 'Yes') !!}

           {!! Form::radio('previous_year', '0', 1) !!}
           {!! Form::label('no', 'No') !!}

         </div>

         <div class="form-group">
              {!! Form::label('group', 'Group', ['class' => 'required']) !!}
              (<small>
                <a target="_blank" href="{{ route('group.create') }}" class="">
                  <i class="fa fa-plus"></i>Add Group
                </a>
              </small>)
              {!! Form::select('group', $groups, 
                    null, 
                    ['class' => 'form-control', 'placeholder' => 'Pick a option...', 'required' => true, 'id' => 'groups']) 
                !!}
              @error('group')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group" id="provider-field" style="display: none;">
            
          </div>

          <div class="form-group">
            {!! Form::label('location', 'Location', ['class' => 'required']) !!}
            {!! Form::select('location', $locations, 
                    null, 
                    ['class' => 'form-control', 'placeholder' => 'Pick a option...', 'required' => true, 'id' => 'location']) 
                !!}
              @error('location')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('year', 'Year', ['class' => 'required']) !!}
            {!! Form::text('year', date('Y'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('year')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">

      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('sra.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
  @if(!empty($sra))
  
  <div class="row">
  </div>
 <div class="row">
         <div class="col-12">
            <div class="card">
             
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Group</th>
                           <th>Providers</th>
                           <th>Location</th>
                           <th>Year</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($sra as $value)
                        <tr>
                           <td>{{ ++$counter }}</td>
                           <td>{{ $value->hasGroup['name'] }}</td>
                           <td>
                              @php $pr = [] @endphp
                               @foreach($value->hasGroup['hasProviders'] as $innerValue)
                                @php $pr[] = $innerValue->name @endphp
                               @endforeach

                               {{ implode(', ', $pr)}}
                           </td>
                           <td>{{ $value->hasLocation['location'] }}</td>
                           <td>{{ $value->year }}</td>
                           <td>
                             <a title="Create new sra from this previous sra?" href="{{ route('sra-map.show', $value->id) }}">Create Copy</a>
                           </td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Group</th>
                           <th>Providers</th>
                           <th>Location</th>
                           <th>Year</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->


@endif
</section>
<!-- /.content -->

<div class="modal fade" id="modal-lg-sra" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header bg-teal">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         {!! Form::open(['id' =>'generate-field']) !!}
         <div class="modal-body">
         </div>
         <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success', 'id' =>'save-field']) !!}
         </div>
      </div>
      <!-- /.modal-content -->
      {!! Form::close() !!}
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



@endsection