
{!! Form::label('provider', 'Provider', ['class' => 'required']) !!} 
 (<small>
     <a target="_blank" href="{{ route('provider.create') }}" class="">
             <i class="fa fa-plus"></i>Add Provider
     </a>
</small>)
{!! Form::select('provider[]', $provider, 
                    null, 
                    ['class' => 'form-control select2','placeholder' => 'Pick a option...', 'multiple' => true]) 
                !!}
@error('provider')
  <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
  </span>
@enderror