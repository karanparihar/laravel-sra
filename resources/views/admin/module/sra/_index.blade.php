@extends('admin.layouts.master')

@section('title', 'Manage SRA')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <a href="{{ route('sra.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create SRA</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Group</th>
                           <th>Providers</th>
                           <th>Previos Year Sra ?</th>
                           <th>Location</th>
                           <th>Year</th>
                           <th>Created At</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($sra as $value)
                        <tr>
                           <td>{{ ++$counter }}</td>
                           <td>{{ $value->hasGroup['name'] }}</td>
                           <td>
                              @php $pr = [] @endphp
                               @foreach($value->hasGroup['hasProviders'] as $innerValue)
                                @php $pr[] = $innerValue->name @endphp
                               @endforeach

                               {{ implode(', ', $pr)}}
                           </td>
                           <td>{{ ($value->is_previous == 1) ? 'YES' : 'NO' }}</td>
                           <td>{{ $value->hasLocation['location'] }}</td>
                           <td>{{ $value->year }}</td>
                           <td>{{ \Carbon\Carbon::parse($value->created_at)->format('d/m/Y') }}</td>
                           <td>{{ !empty($value->status == 1) ? 'Completed' : 'Pending' }}</td>
                           <td>
                              {!! link_to_route('sra.edit',  trans('app.actions.edit'), $parameters = [$value->id ],  ['class' => 'btn btn-secondary']) !!}

                              {!! link_to_route('sra.show',  trans('app.actions.proceed'), $parameters = [$value->id],  ['class' => 'btn btn-success']) !!} 

                               
                           </td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Group</th>
                           <th>Providers</th>
                           <th>Previos Sra ?</th>
                           <th>Location</th>
                           <th>Year</th>
                           <th>Created At</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>


@endsection