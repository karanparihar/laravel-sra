@extends('admin.layouts.master')

@section('title', 'Edit SRA')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => ['sra.update', $sra->id], 'files' => true, 'method'=>'PUT']) !!}
        
        <div class="card-body">
         
         <div class="form-group"> 

           {!! Form::label('previous sra', 'Generate from previous year', ['class' => 'required']) !!} 

           {!! Form::radio('previous_year', '1', !empty($sra->is_previous != NULL) ? true : false, ['disabled' => true]) !!}
           {!! Form::label('yes', 'Yes') !!}

           {!! Form::radio('previous_year', '0', !empty($sra->is_previous == NULL) ? true : false, ['disabled' => true]) !!}
           {!! Form::label('no', 'No') !!}

         </div>

         <div class="form-group">
              {!! Form::label('group', 'Group', ['class' => 'required']) !!}
              (<small>
                <a target="_blank" href="{{ route('group.create') }}" class="">
                  <i class="fa fa-plus"></i>Add Group
                </a>
              </small>)
              {!! Form::select('group', $groups, 
                    $sra->group, 
                    ['class' => 'form-control', 'placeholder' => 'Pick a option...', 'required' => true, 'id' => 'groups']) 
                !!}
              @error('group')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group" id="provider-field" >

            {!! Form::label('provider', 'Provider', ['class' => 'required']) !!} 
               (<small>
                   <a target="_blank" href="{{ route('provider.create') }}" class="">
                           <i class="fa fa-plus"></i>Add Provider
                   </a>
              </small>)
              {!! Form::select('provider[]', $providers, 
                                  explode(',', $sra->provider), 
                                  ['class' => 'form-control select2','placeholder' => 'Pick a option...', 'multiple' => true]) 
                              !!}
              @error('provider')
                <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                </span>
              @enderror
            
          </div>

          <div class="form-group">
            {!! Form::label('location', 'Location', ['class' => 'required']) !!}
            {!! Form::select('location', $locations, 
                    $sra->location, 
                    ['class' => 'form-control', 'placeholder' => 'Pick a option...', 'required' => true, 'id' => 'location']) 
                !!}
              @error('location')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('year', 'Year', ['class' => 'required']) !!}
            {!! Form::text('year', !empty($sra->year) ? $sra->year : date('Y'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('year')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">

      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('sra.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}

</section>
<!-- /.content -->


@endsection