@extends('admin.layouts.master')

@section('title', 'SRA Report')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Year</th>
                           <th>Pending</th>
                           <th>Completed</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($sraReport1 as $key => $value)
                        
                        <tr>
                           <td>{{ ++$counter }}</td>
                           <td>{{ $key }}</td>
                           <td>{{ !empty($value[0]) ? count($value[0]): 0 }}</td>
                           <td>{{ !empty($value[1]) ? count($value[1]): 1 }}</td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Year</th>
                           <th>Pending</th>
                           <th>Completed</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>


@endsection