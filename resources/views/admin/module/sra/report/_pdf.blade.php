
         <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
          
            <div class="row">
               <div class="col-12">
                
                <p>
                  <span>
                      <b>Group:</b> {{ $group }}
                  </span>
                </p>

                <p>
                  <span>
                      <b>Providers:</b> {{ $providers }}
                  </span>
                </p>

                @php $perSection = [] @endphp
                
                @foreach($sraReport->sraMapped as $report)
                   @php $perSection[$report->section_id][] = $report @endphp 
                @endforeach
                
                @if(count($perSection) > 0)
                  @foreach($perSection as $innerKey => $innerValue)
 
                      <div class="post">
                         <div class="user-block">
                            
                            <span>
                            <a href="#">{{ sectionName($innerKey) }} </a>
                            </span>
                            
                         </div>
                         @foreach($innerValue as $innerKey1 => $innerValue1)
                          <p>
                            <span class="">
                              <b>{{ ucwords(str_replace('-', ' ', $innerValue1['meta_name'])) }}</b>:
                            </span>
                            <span class="">{{ $innerValue1['meta_value'] }}</span>
                          </p>
                         
                          @endforeach
                         
                      </div>
                   
                  @endforeach
                @endif

               </div>
            </div>
         </div>
      