@extends('admin.layouts.master')

@section('title', 'SRA Report')

@section('content')

<!-- Main content -->
<section class="content">
<div class="card">

   <div class="card-header">
      <h3 class="card-title">@yield('title')</h3>
    
   </div>

   <div class="card-body">

      <div class="row">

         <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
          
            <div class="row">
               <div class="col-12">
                @php $perSection = [] @endphp
                @foreach($sraReport->sraMapped as $report)
                   @php $perSection[$report->section_id][] = $report @endphp 
                @endforeach
                
                @if(count($perSection) > 0)
                  @foreach($perSection as $innerKey => $innerValue)
 
                      <div class="post">
                         <div class="user-block">
                            
                            <span>
                            <a href="#">{{ sectionName($innerKey) }} </a>
                            </span>
                            
                         </div>
                         @foreach($innerValue as $innerKey1 => $innerValue1)
                          <p>
                            <span class="">
                              <b>{{ ucwords(str_replace('-', ' ', $innerValue1['meta_name'])) }}</b>:
                            </span>
                            <span class="">{{ $innerValue1['meta_value'] }}</span>
                          </p>
                         
                          @endforeach
                         
                      </div>
                   
                  @endforeach
                @endif

               </div>
            </div>
         </div>
         <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
            
            <h5 class="mt-5 text-muted">SRA Documents</h5>
            @if($sraReport->sraDocuments->count() > 0)
            
            <ul class="list-unstyled">
                @php $counter = 0 @endphp
                @foreach($sraReport->sraDocuments as $documents)
                 <li>
                    <a download href="{{ asset('storage/'.$documents->document) }}" class="link-black text-sm"><i class="fas fa-link mr-1"></i>
                       Document {{ ++$counter }}
                    </a>
                 </li>
                @endforeach
            </ul>

            @else
               <div class="text-center mt-5 mb-3">
                 <a href="{{ route('sra.show', $id) }}" class="btn btn-sm btn-primary">Add document</a>
                 
              </div>
            @endif
            
            <a href="{{ route('sra-report.edit', $id) }}" class="btn btn-sm btn-danger"><i class="fa fa-pdf"></i>Generate PDF</a>

            

         </div>
      </div>
   </div>
   <!-- /.card-body -->
</div>
<!-- /.card -->
@endsection