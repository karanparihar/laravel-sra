@extends('admin.layouts.master')

@section('title', 'Completed and Corrective Actions SRA')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Provider</th>
                           <th>Location</th>
                           <th>Created date</th>
                           <th>Corrective actions</th>
                           <th>View report</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($sraCompleted as $key => $value)
                        
                        <tr>
                           <td>{{ ++$counter }}</td>
                           <td>
                               @php $pr = [] @endphp
                               @foreach($value->hasGroup['hasProviders'] as $innerValue)
                                  @php $pr[] = $innerValue->name @endphp
                               @endforeach

                               {{ implode(', ', $pr)}}
                            </td>
                           <td>{{ $value->hasLocation['location'] }}</td>
                           <td>{{ \Carbon\Carbon::parse($value->created_at)->format('d/m/Y') }}</td>
                           <td>
                              {!! link_to_route('sra-report.edit',  trans('app.actions.view'), $parameters = [$value->id ],  ['class' => '']) !!}
                           </td>
                           <td>
                              {!! link_to_route('sra-report.show',  trans('app.actions.view'), $parameters = [$value->id ],  ['class' => '', 'target' => '_blank']) !!}
                           </td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                           <th>S.No</th>
                           <th>Provider</th>
                           <th>Location</th>
                           <th>Created date</th>
                           <th>Corrective actions</th>
                           <th>View report</th>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>


@endsection