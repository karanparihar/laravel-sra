@extends('admin.layouts.master')

@section('title', 'Complete or Create SRA')

@section('content')

@if(Auth::user()->role != 1)
<section class="content">
   <div class="container-fluid">

      <div class="callout callout-info">
            <h5>SRA Status</h5>
            @if($sra->status == 0)
            <h6 class="text-danger">Currently SRA is pending. Click to mark as completed - <a href="javascript:;" onclick="sraStatus(1, {{$sra->id}})">Complete</a></h6>
            @else 
            <h6 class="text-success">SRA is completed.</h6>
            @endif
      </div>

      <div class="row">
         <div class="card-body">

            <div id="accordion">

               @foreach($sections as $key => $section)

               @if(in_array($section->id, $assignedSections))
                   @php $mapped = sraMapped($section->id, $id); @endphp

                   @if($mapped > 0)
                    
                     {!! Form::open(['route' => ['sra-map.update', $section->id], 'files' => true, 'method'=>'PUT']) !!}
                    
                   @else
                    
                     {!! Form::open(['route' => 'sra-map.store', 'files' => true]) !!}

                   @endif

                   <div class="card card-{{ !empty($section->status == 1) ? 'success' : 'danger' }}">
                      <div class="card-header">
                         <h4 class="card-title w-100">
                            <a class="d-block w-100" data-toggle="collapse" href="#collapse{{ readNumber($section->id) }}">
                            {{ $section->name }}
                            </a>
                         </h4>
                      </div>
                      <div id="collapse{{ readNumber($section->id) }}" class="collapse {{ !empty($key == 0) ? 'show' : '' }}" data-parent="#accordion">
                          
                          @php $progress = progressBar($section->id, $id) @endphp
                             
                            <div class="card-body">
                               <div class="progress">
                                  <div class="progress-bar" role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }};">
                                      <span class="sr-only">{{ $progress }} Complete</span>
                                  </div>
                                  <span class="progress-type"></span>
                                  <span class="progress-completed">{{ $progress }}</span>
                            </div>


                            @if($section->formFields->count() > 0)
                            
                            <input type="hidden" name="sra_id" value="{{ $id }}">
                            
                            <input type="hidden" name="section_id" value="{{ $section->id }}">

                            @foreach($section->formFields as $data)
                      
                            <!------------ TEXT -------------->
                            @if($data->field_type =='text')
                            
                            @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._input', compact('data', 'sraMappedData'))

                            <!------------ EMAIL -------------->
                            @elseif($data->field_type =='email')
           
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._input', $data)

                            <!------------ URL -------------->
                            @elseif($data->field_type == 'url')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._url', 
                            $data)

                            <!------------ NUMBER -------------->
                            @elseif($data->field_type == 'number')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._number', 
                            $data) 

                            <!------------ TEXTAREA -------------->
                            @elseif($data->field_type == 'textarea')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._textarea', $data) 

                            <!------------ RADIO BUTTONS -------------->
                            @elseif($data->field_type == 'radio-buttons')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._radio', $data) 

                            <!------------ CHECKBOXES -------------->
                            @elseif($data->field_type == 'checkboxes')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._checkbox', $data)

                            <!------------ DROP DOWN -------------->
                            @elseif($data->field_type == 'drop-down')
                            
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._select', 
                            $data)

                            <!------------ FILE -------------->
                            @elseif($data->field_type == 'file')
                             
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                      
                            @include('admin.module.sra.fields._file', 
                            $data)

                            <!------------ TEL -------------->
                            @elseif($data->field_type == 'tel')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                            

                            @include('admin.module.sra.fields._tel', 
                            $data)
                             
                            <!------------ DATE -------------->
                            @elseif($data->field_type == 'date')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                            
                            @include('admin.module.sra.fields._date', 
                            $data)

                            @else

                            @endif
                            
                            @endforeach
                           
                            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-primary']) !!}

                            @else
                            
                            @endif

                         </div>
                      </div>
                      
                      {!! Form::close() !!}

                   </div>

                @endif

               @endforeach
            </div>

             <div id="accordion">
                  <div class="card card-danger">
                    <div class="card-header">
                      <h4 class="card-title w-100">
                        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                          @php $documents = documents($id) @endphp
                          Link Documents ({{ $documents }})
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                      <div class="card-body">
                        {!! Form::open(['route' => 'sra-document.store', 'files' => true]) !!}

                          {!! Form::hidden('sra_id', $id) !!}
                          
                          <div class="form-group">
                            
                            {!! Form::label('document', 'Upload document' ) !!}  

                            {!! Form::file('sra_document[]' , [
                             'class' => 'form-control', 
                             'required' => true,
                             'id' => '', 
                             'size'    => '',
                             'multiple' => true
                     
                           ])  !!}
                          </div> 
                        {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}

                      </div>
                    </div>
                  </div>
                 
                </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@else

<section class="content">
   <div class="container-fluid">

      <div class="callout callout-info">
            <h5>SRA Status</h5>
            @if($sra->status == 0)
            <h6 class="text-danger">Currently SRA status is pending. Click to mark as completed - <a href="javascript:;" onclick="sraStatus(1, {{$sra->id}})">Complete</a></h6>
            @else 
            <h6 class="text-success">SRA is completed.</h6>
            @endif
      </div>

      <div class="row">
         <div class="card-body">

            <div id="accordion">

               @foreach($sections as $key => $section)

             
                   @php $mapped = sraMapped($section->id, $id); @endphp

                   @if($mapped > 0)
                    
                     {!! Form::open(['route' => ['sra-map.update', $section->id], 'files' => true, 'method'=>'PUT']) !!}
                    
                   @else
                    
                     {!! Form::open(['route' => 'sra-map.store', 'files' => true]) !!}

                   @endif

                   <div class="card card-{{ !empty($section->status == 1) ? 'success' : 'danger' }}">
                      <div class="card-header">
                         <h4 class="card-title w-100">
                            <a class="d-block w-100" data-toggle="collapse" href="#collapse{{ readNumber($section->id) }}">
                            {{ $section->name }}
                            </a>
                         </h4>
                      </div>
                      <div id="collapse{{ readNumber($section->id) }}" class="collapse {{ !empty($key == 0) ? 'show' : '' }}" data-parent="#accordion">
                          
                          @php $progress = progressBar($section->id, $id) @endphp
                             
                            <div class="card-body">
                               <div class="progress">
                                  <div class="progress-bar" role="progressbar" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }};">
                                      <span class="sr-only">{{ $progress }} Complete</span>
                                  </div>
                                  <span class="progress-type"></span>
                                  <span class="progress-completed">{{ $progress }}</span>
                            </div>


                            @if($section->formFields->count() > 0)
                            
                            <input type="hidden" name="sra_id" value="{{ $id }}">
                            
                            <input type="hidden" name="section_id" value="{{ $section->id }}">

                            @foreach($section->formFields as $data)
                      
                            <!------------ TEXT -------------->
                            @if($data->field_type =='text')
                            
                            @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._input', compact('data', 'sraMappedData'))

                            <!------------ EMAIL -------------->
                            @elseif($data->field_type =='email')
           
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._input', $data)

                            <!------------ URL -------------->
                            @elseif($data->field_type == 'url')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._url', 
                            $data)

                            <!------------ NUMBER -------------->
                            @elseif($data->field_type == 'number')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._number', 
                            $data) 

                            <!------------ TEXTAREA -------------->
                            @elseif($data->field_type == 'textarea')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._textarea', $data) 

                            <!------------ RADIO BUTTONS -------------->
                            @elseif($data->field_type == 'radio-buttons')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._radio', $data) 

                            <!------------ CHECKBOXES -------------->
                            @elseif($data->field_type == 'checkboxes')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._checkbox', $data)

                            <!------------ DROP DOWN -------------->
                            @elseif($data->field_type == 'drop-down')
                            
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp

                            @include('admin.module.sra.fields._select', 
                            $data)

                            <!------------ FILE -------------->
                            @elseif($data->field_type == 'file')
                             
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                      
                            @include('admin.module.sra.fields._file', 
                            $data)

                            <!------------ TEL -------------->
                            @elseif($data->field_type == 'tel')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                            

                            @include('admin.module.sra.fields._tel', 
                            $data)
                             
                            <!------------ DATE -------------->
                            @elseif($data->field_type == 'date')
                            
                             @php $sraMappedData = fieldData($section->id, $id, $data->id) @endphp
                            
                            @include('admin.module.sra.fields._date', 
                            $data)

                            @else

                            @endif
                            
                            @endforeach
                           
                            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-primary']) !!}

                            @else
                            
                            @endif

                         </div>
                      </div>
                      
                      {!! Form::close() !!}

                   </div>

               

               @endforeach
            </div>

             <div id="accordion">
                  <div class="card card-danger">
                    <div class="card-header">
                      <h4 class="card-title w-100">
                        <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                          @php $documents = documents($id) @endphp
                          Link Documents ({{ $documents }})
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                      <div class="card-body">
                        {!! Form::open(['route' => 'sra-document.store', 'files' => true]) !!}

                          {!! Form::hidden('sra_id', $id) !!}
                          
                          <div class="form-group">
                            
                            {!! Form::label('document', 'Upload document' ) !!}  

                            {!! Form::file('sra_document[]' , [
                             'class' => 'form-control', 
                             'required' => true,
                             'id' => '', 
                             'size'    => '',
                             'multiple' => true
                     
                           ])  !!}
                          </div> 
                        {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-primary']) !!}
                        {!! Form::close() !!}

                      </div>
                    </div>
                  </div>
                 
                </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@endif

@endsection