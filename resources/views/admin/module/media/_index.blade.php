@extends('admin.layouts.master')

@section('title', 'Media')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <a href="{{ route('media.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Upload Media</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Location</th>
                           <th>Media Count</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($media as $value)
                       
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value[0]->hasLocation['location'] }}</td>
                           <td>{{ $value->count() }}</td>
                           <td>
                           	  
                            {!! link_to_route('media.show',  trans('app.actions.view'), $parameters = [$value[0]->location ],  ['class' => 'btn btn-secondary']) !!}

                           </td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Location</th>
                           <th>Media Count</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>

@endsection