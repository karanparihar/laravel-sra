@extends('admin.layouts.master')

@section('title', $location[0] . ' Media Gallery')

@section('content')

 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">

          <div class="col-12">
            <div class="card card-primary">
              
              <div class="card-body">
                <div>
                  <div class="btn-group w-100 mb-2">
                    <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>

                    @foreach($sraType as $key => $sra)
                       <a class="btn btn-info" href="javascript:void(0)" data-filter="{{ $sra->id }}"> {{ $sra->name }}</a>
                    @endforeach

                  </div>
                  <div class="mb-2">
                    <a class="btn btn-secondary" href="javascript:void(0)" data-shuffle> Shuffle items </a>
                    <div class="float-right">
                      <select class="custom-select" style="width: auto;display: none;" data-sortOrder>
                        <option value="index"> Sort by Position </option>
                        <option value="sortData"> Sort by Custom Data </option>
                      </select>
                      <div class="btn-group">
                        <a class="btn btn-default" href="javascript:void(0)" data-sortAsc> Ascending </a>
                        <a class="btn btn-default" href="javascript:void(0)" data-sortDesc> Descending </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <div class="filter-container p-0 row">
                   
                   @foreach($media as $innerKey => $innerValue)

                    <div class="filtr-item col-sm-2" data-category="{{ $innerValue->hasSraType['id'] }}" data-sort="white sample">
                      <a href="{{ asset('storage/'.$innerValue->media) }}" data-toggle="lightbox" data-title="{{ $innerValue->hasSraType['name'] }}">
                        <img src="{{ asset('storage/'.$innerValue->media) }}" class="img-fluid mb-2" alt="Image" style="width: 181.4px !important; height: 181.4px !important" />
                      </a>
                    </div>

                    @endforeach
               
                  </div>
                </div>

              </div>
            </div>
          </div>
         
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection