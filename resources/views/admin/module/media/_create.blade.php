@extends('admin.layouts.master')

@section('title', 'Upload Media')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>

        {!! Form::open(['route' => 'media.store', 'files' => true]) !!}
        <div class="card-body">
         
           <div class="form-group">
              {!! Form::label('location', 'Location', ['class' => 'required']) !!}
              {!! Form::select('location', $locations, 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...']) 
                !!}
              @error('location')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

         <div class="form-group">
              {!! Form::label('type', 'Sra type', ['class' => 'required']) !!}
              {!! Form::select('sra', $SraTypeAttr, 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...']) 
                !!}
              @error('location')
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
              {!! Form::label('file', 'File', ['class' => 'required']) !!}
              {!! Form::file('media[]' , ['class' => 'form-control', 'multiple' => true])  !!}
              @error('media' )
                  <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>


        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>

   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('media.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection