@extends('admin.layouts.master')

@section('title', 'Manage Form')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="card card-success card-outline">
               <div class="card-header">
                  <h3 class="card-title">
                     <i class="fas fa-edit"></i>
                     Create the form template and fields here.
                  </h3>
               </div>
               <div class="card-body pad table-responsive">
                  <table class="table text-center field-listing">
                     <tr>
                        <td>
                           <button onclick="addForm('text');" type="button" class="btn btn-block btn-success">Text</button>
                        </td>
                        <td>
                           <button onclick="addForm('email');" type="button" class="btn btn-block btn-info">Email</button>
                        </td>
                        <td>
                           <button onclick="addForm('textarea');" type="button" class="btn btn-block btn-primary">Text area</button>
                        </td>
                        <td>
                           <button onclick="addForm('url');" type="button" class="btn btn-block btn-warning">URL</button>
                        </td>
                        <td>
                           <button onclick="addForm('number');" type="button" class="btn btn-block btn-danger">Number</button>
                        </td>
                        <td>
                           <button onclick="addForm('drop-down');" type="button" class="btn btn-block btn-default bg-purple">Drop-down menu</button>
                        </td>
                        
                        <td>
                           <button onclick="addForm('checkboxes');" type="button" class="btn btn-block btn-default bg-teal">Checkboxes</button>
                        </td>
                        <td>
                           <button onclick="addForm('radio-buttons');" type="button" class="btn btn-block btn-default bg-pink">Radio buttons</button>
                        </td>
                        <!-- <td>
                           <button onclick="addForm('file');" type="button" class="btn btn-block btn-default bg-orange">File</button>
                        </td> -->
                        <td>
                           <button onclick="addForm('tel');" type="button" class="btn btn-block btn-default bg-olive">Tel</button>
                        </td>
                        <td>
                           <button onclick="addForm('date');" type="button" class="btn btn-block btn-default bg-navy">Date</button>
                        </td>
                     </tr>
                  </table>
               </div>
               <!-- /.card -->
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- ./row -->
      <div class="row">
         <div class="col-md-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  <h3 class="card-title">
                     <i class="fas fa-edit"></i>
                     Create the form template and fields here.
                  </h3>
               </div>
               <div class="card-body pad table-responsive">
                  <table id="example1" class="table table-bordered text-center">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Required</th>
                           <th>Field type</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php  $counter = 0 @endphp
                        @foreach($formFields as $key => $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->name }}</td>
                          
                           <td>{{ ($value->required == 1) ? 'on' : 'off'  }}</td>
                           <td>{{ $value->field_type }}</td>
                           <td>
                              @if($value->status == 1)
                                  <a href="javascript:;" onclick="activateDeactivate(0, {{ $value->id }});" title="Click to deactivate the field" class="text-success">{{ trans('app.actions.active') }}</a>
                              @else
                                 <a href="javascript:;" onclick="activateDeactivate(1, {{ $value->id }});" title="Click to activate the field" class="text-danger">{{ trans('app.actions.inactive') }}</a>
                              @endif
                           </td>
                           <td>
                              {!! link_to_route('manage-form.edit',  trans('app.actions.edit'), $parameters = [$value->id ],  ['class' => 'btn btn-secondary']) !!}
                              
                              <button onclick="viewFormField({{ $value->id }});" type="button" class="btn btn-success">{{ trans('app.actions.view') }}</button>

                              <button onclick="commonDeleteAjax('{{ route('manage-form.destroy', $value->id)}}');" type="button" class="btn btn-danger">{{ trans('app.actions.trash') }}</button>


                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Required</th>
                           <th>Field type</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card -->
            </div>
         </div>
         <!-- /.col -->
      </div>
   </div>
   <!-- /.container-fluid -->
</section>

<!-- /.Create form Modal -->
  
  @include('admin.module.form-builder.modals.create-form')

<!-- /.View form Field Modal -->

  @include('admin.module.form-builder.modals.view-form')

@endsection