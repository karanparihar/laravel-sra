
<table width="100%" class="ajax-form-table">
    
   @include('admin.module.form-builder.includes._common_upper')

   @if($attribute == 'text' || $attribute == 'email'  || $attribute == 'textarea' || $attribute == 'number')
    
       @include('admin.module.form-builder.includes._ajax-inputs')

   @endif

   @if($attribute == 'drop-down')

       @include('admin.module.form-builder.includes._ajax-drop-down')

   @endif

   @if($attribute == 'checkboxes' || $attribute == 'radio-buttons' )

       @include('admin.module.form-builder.includes._ajax-checkbox-radio')

   @endif

   @if($attribute == 'number')
        
       @include('admin.module.form-builder.includes._ajax-number')

   @endif

    @if($attribute == 'url')
        
       @include('admin.module.form-builder.includes._ajax-url')

   @endif

   @if($attribute == 'file')
        
       @include('admin.module.form-builder.includes._ajax-file')

   @endif

   @if($attribute == 'tel')
        
       @include('admin.module.form-builder.includes._ajax-tel')

   @endif

   @if($attribute == 'date')
        
       @include('admin.module.form-builder.includes._ajax-date')

   @endif
   
       @include('admin.module.form-builder.includes._common')
   
  {!! Form::hidden('field_type', $attribute) !!}

</table>

