<table border="1"  width="100%">
   <thead>
      <tr>
         @for($j = 1; $j <= $cols; $j++)
         <th contenteditable="true" width="{{ $width }}%"></th>
         @endfor
      </tr>
   </thead>
   <tbody>
      @for($i = 1; $i <= $rows - 1; $i++)
      <tr>
         @for($j = 1; $j <= $cols; $j++)
         <td contenteditable="true" width="{{ $width }}%"></td>
         @endfor
      </tr>
      @endfor
   </tbody>
</table>