@extends('admin.layouts.master')
@section('title', 'Add Form Field')
@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="card card-primary card-outline">
               <div class="card-header">
                  <h3 class="card-title">
                     <i class="fas fa-edit"></i>
                     Form
                  </h3>
               </div>
               <div class="card-body pad table-responsive">
                  <p>You can edit the form template here</p>
                  <table class="table table-bordered text-center">
                    
                     <tr>
                        <td>
                           <button onclick="addForm('text');" type="button" class="btn btn-block btn-primary">Text</button>
                        </td>
                        <td>
                           <button onclick="addForm('email');" type="button" class="btn btn-block btn-default">Email</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-default">Default</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-default">Default</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-default">Default</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-default">Default</button>
                        </td>
                     </tr>
                    
                  </table>
               </div>
               <!-- /.card -->
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- ./row -->
      
   </div>
   <!-- /.container-fluid -->
</section>

  <div class="modal fade" id="modal-lg" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            {!! Form::open(['id' =>'generate-field']) !!}
            <div class="modal-body">
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success', 'id' =>'save-field']) !!}
            </div>
          </div>
          <!-- /.modal-content -->
          {!! Form::close() !!}
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
@endsection