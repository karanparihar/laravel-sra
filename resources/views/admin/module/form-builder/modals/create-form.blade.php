<div class="modal fade" id="modal-lg" data-backdrop="static" data-keyboard="false">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header bg-teal">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         {!! Form::open(['id' =>'generate-field']) !!}
         <div class="modal-body">
         </div>
         <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success', 'id' =>'save-field']) !!}
         </div>
      </div>
      <!-- /.modal-content -->
      {!! Form::close() !!}
   </div>
   <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
