@if($data->required == "1") 
   @php $required = true @endphp
@else
   @php $required = false @endphp
@endif


@if($data->include_blank == "1") 
   @php $placeholder = trans('app.dropdown.option') @endphp
@else
   @php $placeholder = '' @endphp
@endif

@if($data->multiple == "1") 
   @php $multiple = "multiple"; $multiple_class = "select2"; $arr = "[]"; @endphp
@else
   @php $multiple = ""; $multiple_class = ""; $arr = ""; @endphp
@endif

@if($data->status == "1") 
   @php $activeInactive = 'Active'; $class = 'text-success' @endphp
@else
     @php $activeInactive = 'InActive'; $class = 'text-danger' @endphp
@endif


<div class="form-group">
    {!! Form::label($data->name, ucwords($data->name)) !!} 

    @if(Route::currentRouteName() == 'sections.show' || Route::currentRouteName() == 'users.show')
      <sup class="{{ $class }}">[{{ $activeInactive }}]</sup>
   @endif

    <br>
    
    @php $options = explode(PHP_EOL  , $data->values ) @endphp

    {!! Form::select(str_slug($data->name).$arr, $options , null, 
         ['class' => $multiple_class.' form-control '.$data->class_attribute, 
          'id'   => $data->id_attribute,
          'placeholder' => $placeholder, 
          'required' => $required, 
          $multiple ]
        ) 
    !!}
</div>