
@if($data->required == "1") 
   @php $required = true @endphp
@else
   @php $required = false @endphp
@endif


@if($data->placeholder == "1") 
   @php $placeholder = $data->values @endphp
@else
   @php $placeholder = '' @endphp
@endif

@if($data->status == "1") 
   @php $activeInactive = 'Active'; $class = 'text-success' @endphp
@else
     @php $activeInactive = 'InActive'; $class = 'text-danger' @endphp
@endif


<div class="form-group">
   {!! Form::label($data->name, ucwords($data->name)) !!} 
    
    @if(Route::currentRouteName() == 'sections.show' || Route::currentRouteName() == 'users.show' )
      <sup class="{{ $class }}">[{{ $activeInactive }}]</sup>
    @endif
   <br>
   
   @php $options = explode(PHP_EOL  , $data->values ) @endphp
   
   @if($data->label_first == "1")
	   @foreach($options as $option)
	     
	      {!! Form::label($option, ucwords($option)) !!} 
	      {!! Form::checkbox(str_slug($data->name).'[]', $option , '' , ['id' => $data->id_attribute, 'class' => $data->class_attribute ]) !!}
	      
	   @endforeach
	@else
       @foreach($options as $option)
         
	      {!! Form::checkbox(str_slug($data->name).'[]', $option , '' , ['id' => $data->id_attribute, 'class' => $data->class_attribute ]) !!}
	      {!! Form::label($option, ucwords($option)) !!} 

	   @endforeach
	@endif
  
</div>