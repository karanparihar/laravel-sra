
<!--------------- TEXT -------------->

@if($data->field_type == 'text') 
  
  @include('admin.module.form-builder.fields._input')

<!--------------- EMAIL -------------->

@elseif($data->field_type == 'email')
  
   @include('admin.module.form-builder.fields._email')

<!--------------- URL -------------->

@elseif($data->field_type == 'url')
 
   @include('admin.module.form-builder.fields._url')

<!--------------- NUMBER -------------->

@elseif($data->field_type == 'number')
 
   @include('admin.module.form-builder.fields._number')

<!--------------- TEXTAREA -------------->

@elseif($data->field_type == 'textarea')

   @include('admin.module.form-builder.fields._textarea')

<!--------------- RADIO BUTTONS -------------->

@elseif($data->field_type == 'radio-buttons')
  
   @include('admin.module.form-builder.fields._radio')

<!--------------- CHECKBOX -------------->

@elseif($data->field_type == 'checkboxes')
 
   @include('admin.module.form-builder.fields._checkbox')

<!--------------- DROP DOWN -------------->

@elseif($data->field_type == 'drop-down')
   
   @include('admin.module.form-builder.fields._select')

<!--------------- FILE -------------->

@elseif($data->field_type == 'file')

   @include('admin.module.form-builder.fields._file')

<!--------------- TEL -------------->

@elseif($data->field_type == 'tel')

   @include('admin.module.form-builder.fields._tel')

<!--------------- DATE -------------->

@elseif($data->field_type == 'date')

   @include('admin.module.form-builder.fields._date')


@else


@endif