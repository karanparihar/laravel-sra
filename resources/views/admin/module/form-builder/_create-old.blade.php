@extends('admin.layouts.master')
@section('title', 'Add Form Field')
@section('content')
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">@yield('title')</h3>
            </div>
            <div class="card-body">
               <div class="form-group col-6">
                  {!! Form::label('status', 'Select to create', ['class' => 'required']) !!}
                  {!! Form::select('status', [
                  '1' => trans('app.dropdown.table'), 
                  '2' => trans('app.dropdown.form')
                  ], 
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a option...', 'id' => 'select-builder']) 
                  !!}
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
      </div>
   </div>
   <div class="row" id="create-table-layout" style="display: none;">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Create Table Layout</h3>
            </div>
            <div class="card-body">
               <div class="form-group">
                  {!! Form::label('rows', 'Select to row', ['class' => 'required']) !!}
                  {!! Form::select('status', $rows_cols,
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a option...', 'id' => 'select-rows', 'id' => 'row-val', 'required' => true]) 
                  !!}
                  {!! Form::label('cols', 'Select to columns', ['class' => 'required']) !!}
                  {!! Form::select('status', $rows_cols,
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a option...', 'id' => 'select-cols', 'id' => 'col-val', 'required' => true]) 
                  !!}
               </div>
               <button id="create-table" class="btn btn-primary">Create Table</button> 
               <div class="form-group col-5">
               </div>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
      </div>
   </div>
   <div class="row" id="table-layout" style="display: none;">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Table Layout</h3>
            </div>
            <div class="card-body">
               {!! Form::open(['route' => 'manage-form.store', 'files' => true]) !!}
               <div class="form-group">
                  {!! Form::label('section', 'Assign Section', ['class' => 'required']) !!}
                  {!! Form::select('section', $selectAttr,
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a section...', 'id' => 'select-builder']) 
                  !!}
               </div>
               <div class="table-layout">
               </div>
               <div class="row">
                  <div class="col-12">
                     {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
                     {!! link_to_route('sections.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
                  </div>
               </div>
               {!! Form::close() !!}
            </div>
         </div>
         <!-- /.card -->
      </div>
   </div>

   <div class="row" id="create-form-layout" style="display: none;">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Create Form Layout</h3>
            </div>
            <div class="card-body">

               <div class="form-group col-6">
                
                  {!! Form::label('Field Type', 'Field Type', ['class' => 'required']) !!}
                  {!! Form::select('type', [
                    '1' => 'Input Box', 
                    '2' => 'Textarea',
                    '3' => 'Checkbox',
                    '4' => 'Radio',
                    '5' => 'Number'
                  ], 
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a option...', 'required' => 'true' , 'id' => 'field-type']) 
                  !!}
               </div>

                <div class="form-group col-6" id="if-checkbox-radio" style="display: none;">
                   
                  {!! Form::label('loop', 'Radio/checkbox count', ['class' => 'required']) !!}
                    {!! Form::select('type', $rows_cols, 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...', 'required' => 'true', 'id' => 'loop']) 
                    !!}
               </div>

              <div class="form-group col-6">
                  
                    {!! Form::label('Required', 'Required', ['class' => 'required']) !!}
                    {!! Form::select('type', [
                      'true' => 'true', 
                      'false' => 'false',
                    ], 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick a option...', 'required' => 'true', 'id' =>'required']) 
                    !!}
                  
               </div>
               
               <button id="create-form-field" class="btn btn-primary">Create Form Field</button> 
             
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
      </div>
   </div>

   <div class="row" id="form-layout" style="display: none;">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">Table Layout</h3>
            </div>
            <div class="card-body">
               {!! Form::open(['route' => 'manage-form.store', 'files' => true]) !!}
               <div class="form-group">
                  {!! Form::label('section', 'Assign Section', ['class' => 'required']) !!}
                  {!! Form::select('section', $selectAttr,
                  null, 
                  ['class' => 'form-control','placeholder' => 'Pick a section...', 'id' => 'select-builder']) 
                  !!}
               </div>
               <div class="form-layout">
               </div>
               <div class="row">
                  <div class="col-12">
                     {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
                     {!! link_to_route('sections.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
                  </div>
               </div>
               {!! Form::close() !!}
            </div>
         </div>
         <!-- /.card -->
      </div>
   </div>

</section>
<!-- /.content -->
@endsection