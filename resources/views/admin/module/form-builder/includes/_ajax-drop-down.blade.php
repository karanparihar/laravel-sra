<tr>
   <td width="25%">{!! Form::label('options', 'Options') !!}</td>
   <td width="75%">
      <small>One option per line.</small>
      {!! Form::textarea('values', !empty($manage_form->values) ? $manage_form->values : '', ['class' => 'form-control', 'placeholder' => '', 'rows' => 3, 'cols' => 4, 'required' => true]) !!}
   </td>
</tr>
<tr>
   <td width="25%"></td>
   <td width="75%">{!! Form::checkbox('multiple', '1', !empty($manage_form->multiple) ? true : false ) !!}
      {!! Form::label('multiple', 'Allow multiple selections', ['class' => 'form-label']) !!}
   </td>
</tr>
<tr>
   <td width="25%"></td>
   <td width="75%">{!! Form::checkbox('include_blank', '1', !empty($manage_form->include_blank) ? true : false) !!}
      {!! Form::label('required', 'Insert a blank item as the first option', ['class' => 'form-label']) !!}
   </td>
</tr>