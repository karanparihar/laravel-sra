<tr>
   <td width="25%"> {!! Form::label('default value', 'File size limit (bytes)') !!}</td>
   <td width="75%"> {!! Form::text('limit', !empty($manage_form->limit) ? $manage_form->limit : '', ['class' => 'form-control', 'placeholder' => '']) !!}</td>
</tr>
<tr>
   <td width="25%"> {!! Form::label('default value', 'Acceptable file types') !!}</td>
   <td width="75%"> {!! Form::text('filetypes', !empty($manage_form->filetypes) ? $manage_form->filetypes : '', ['class' => 'form-control', 'placeholder' => '']) !!}</td>
</tr>