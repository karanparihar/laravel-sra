<tr>
   <td width="25%">{!! Form::label('range', 'Range') !!}</td>
   <td width="75%"> 
      <label> Min: {!! Form::number('min', !empty($manage_form->min) ? $manage_form->min : '', array_merge(['class' => 'form-control', 'required' => 'true'])) !!}
      </label>
      <label> Max: {!! Form::number('max', !empty($manage_form->max) ? $manage_form->max : '' , array_merge(['class' => 'form-control', 'required' => 'true'])) !!}
      </label>
   </td>
</tr>