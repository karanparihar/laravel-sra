<tr>
   <td width="25%"> {!! Form::label('id attribute', 'Id attribute') !!}</td>
   <td width="75%"> {!! Form::text('id_attribute', !empty($manage_form->id_attribute) ? $manage_form->id_attribute : '', ['class' => 'form-control', 'placeholder' => '']) !!}</td>
</tr>
<tr>
   <td width="25%"> {!! Form::label('class attribute', 'Class attribute') !!}</td>
   <td width="75%"> {!! Form::text('class_attribute', !empty($manage_form->class_attribute) ? $manage_form->class_attribute : '', ['class' => 'form-control', 'placeholder' => '']) !!}</td>
</tr>

