<tr>
   <td width="25%">{!! Form::label('options', 'Options') !!}</td>
   <td width="75%">
      <small>One option per line.</small>
      {!! Form::textarea('values', !empty($manage_form->values) ? $manage_form->values : '', ['class' => 'form-control', 'placeholder' => '', 'rows' => 3, 'cols' => 4, 'required' => true]) !!}
   </td>
</tr>
<tr>
   <td width="25%"></td>
   <td width="75%">{!! Form::checkbox('label_first', '1', !empty($manage_form->label_first) ? true : false) !!}
      {!! Form::label('multiple', 'Put a label first, a checkbox last', ['class' => 'form-label']) !!}
   </td>
</tr>