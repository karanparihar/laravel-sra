<tr>
   <td width="25%"> {!! Form::label('default value', 'Default Value') !!}</td>
   <td width="75%"> {!! Form::text('values', !empty($manage_form->values) ? $manage_form->values : '', ['class' => 'form-control', 'placeholder' => 'DD-MM-YYYY']) !!}</td>
</tr>
<tr>
   <td width="25%"> </td>
   <td width="75%"> 
      {!! Form::checkbox('placeholder', '1', !empty($manage_form->placeholder) ? true : false) !!}
      {!! Form::label('required', 'Use default value text as the placeholder of the field.') !!}
   </td>
</tr>