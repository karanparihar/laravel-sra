<tr>
   <div class="callout callout-info">
      <p>Generate a form-tag for a {{ $attribute }} input field.</p>
   </div>
</tr>
<tr>
	<td width="25%">{!! Form::label('section', 'Section') !!}</td>
	<td width="75%"><small class="text-info">Only active sections listed here.</small>
    {!! Form::select('section_id', $sections , !empty($manage_form->section_id) ? $manage_form->section_id: '' , 
         ['class' => 'form-control', 
          'placeholder' => 'Select section', 
          'required' => true, 
          ]
        ) 
    !!}</td>
</tr>
<tr>
   <td width="25%">{!! Form::label('field type', 'Field Type') !!}</td>
   <td width="75%">{!! Form::checkbox('required', '1', !empty($manage_form->required) ? true : false) !!}
      {!! Form::label('required', 'Required field', ['class' => 'form-label']) !!}
   </td>
</tr>
<tr>
   <td width="25%"> {!! Form::label('name', 'Name') !!}</td>
   <td width="75%"> {!! Form::text('name', !empty($manage_form->name) ? $manage_form->name: '', ['class' => 'form-control', 'placeholder' => $attribute, 'required' => 'true']) !!}</td>
</tr>