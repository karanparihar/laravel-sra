@extends('admin.layouts.master')

@section('title', 'Edit Form Field')

@section('content')
<!-- Main content -->
<section class="content">
   <div class="row">
      <div class="col-md-10">
         <div class="card card-primary">
            <div class="card-header">
               <h3 class="card-title">@yield('title')</h3>
            </div>
            {!! Form::open(['route' => ['manage-form.update', $manage_form->id], 'files' => true, 'method'=>'PUT']) !!}
            <div class="card-body">
               <table width="100%" class="ajax-form-table">

                  @include('admin.module.form-builder.includes._common_upper')

                  @if($attribute == 'text' || $attribute == 'email'  || $attribute == 'textarea' || $attribute == 'number')
                      @include('admin.module.form-builder.includes._ajax-inputs')
                  @endif

                  @if($attribute == 'drop-down')
                      @include('admin.module.form-builder.includes._ajax-drop-down')
                  @endif

                  @if($attribute == 'checkboxes' || $attribute == 'radio-buttons' )
                      @include('admin.module.form-builder.includes._ajax-checkbox-radio')
                  @endif

                  @if($attribute == 'number')
                       @include('admin.module.form-builder.includes._ajax-number')
                  @endif

                  @if($attribute == 'file')
                       @include('admin.module.form-builder.includes._ajax-file')
                  @endif

                  @if($attribute == 'tel')
                       @include('admin.module.form-builder.includes._ajax-tel')
                  @endif

                  @if($attribute == 'date')
                       @include('admin.module.form-builder.includes._ajax-date')
                  @endif

                  @include('admin.module.form-builder.includes._common')

                  {!! Form::hidden('field_type', $attribute) !!}
               </table>
            </div>
            <!-- /.card-body -->
         </div>
         <!-- /.card -->
      </div>
   </div>
   <div class="row">
      <div class="col-12">
         {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
         {!! link_to_route('manage-form.index',  'Cancel', $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
      </div>
   </div>
   {!! Form::close() !!}
</section>
<!-- /.content -->
@endsection