@extends('admin.layouts.master')

@section('title', 'Section Ordering')

@section('content')

<style type="text/css">
   .slist {
   list-style: none;
   padding: 0;
   margin: 0;
   }
   .slist li {
   margin: 5px;
   padding: 10px;
   border: 1px solid #333;
   background: #eaeaea;
   }
   /* (B) DRAG-AND-DROP HINT */
   .slist li.hint { background: #fea; }
   .slist li.active { background: #ffd4d4; }
</style>
<section class="content" id="section-sorting" data-count="{{ $sraType->count() }}">
   <div class="container-fluid">
      <div class="callout callout-info">
            <h5>Drag and Drop</h5>
            <p>Drag and Drop up down to set the order of Active sections.</p>
       </div>
   
      <div class="row">
      
         @foreach($sraType as $outerKey => $sra)
         <div class="col-6">
            <div class="card">
               <div class="card-header bg-info color-palette">
                  <h3 class="card-title">
                     <i class="fas fa-puzzle-piece"></i>
                      {{ $sra->name }} 
                  </h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div class="tab-content p-0">
                     <!-- Morris chart - Sales -->
                     <ul id="sortlist-{{$outerKey}}">

                     @foreach($activeSections as $section)
                        @if($section->sra == $sraType[$outerKey]->id)
                            <li data-id="{{ $section->id }}" data-order="{{ $section->ordering }}">{{ $section->name }}</li>
                        @endif
                     @endforeach
                     </ul>
                  </div>
                  
               </div>
               <!-- /.card-body -->
            </div>
         </div>
       
         <!-- /.col -->

          @endforeach

      
         <!-- /.col -->
      </div>
     
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection