@extends('admin.layouts.master')

@section('title', 'Groups')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <a href="{{ route('group.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add Group</a>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     @php $counter = 0; @endphp
                     @foreach($groups as $value)
                        <tr>
                           <td>#{{ ++$counter }}</td>
                           <td>{{ $value->name }}</td>
                           <td>{{ $value->description }}</td>
                           <td>
                           	{!! link_to_route('group.edit',  trans('app.actions.edit'), $parameters = [$value->id ],  ['class' => 'btn btn-secondary']) !!}                              
                  
                           </td>
                        </tr>
                     @endforeach
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>S.No</th>
                           <th>Name</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                  </table>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>


@endsection