@extends('admin.layouts.master')

@section('title', 'Profile Setting')

@section('content')

<section class="content">
   <div class="container-fluid">
      <div class="row">
        

          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-warning">
               <div class="card-header">
                  <h3 class="card-title">Change Picture</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form>
                  <div class="card-body">
                   
                     <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <div class="input-group">
                           <div class="custom-file">
                              <input type="file" class="custom-file-input" id="exampleInputFile">
                              <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                           </div>
                           <div class="input-group-append">
                              <span class="input-group-text">Upload</span>
                           </div>
                        </div>
                     </div>
                    
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </form>
            </div>
            <!-- /.card -->
         </div>
      </div>
   </div>
</section>

@endsection