@extends('admin.layouts.master')

@section('title', 'Change Password')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => 'change-password.store', 'files' => true]) !!}
        <div class="card-body">

          <div class="form-group">
            {!! Form::label('current-password', 'Current Password', ['class' => 'required']) !!}
            {!! Form::password('current_password', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('current_password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('password', 'New Password', ['class' => 'required']) !!}
            {!! Form::password('password', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('password-confirm', 'Confirm Password', ['class' => 'required']) !!}
            {!! Form::password('password_confirmation', array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
            @error('password_confirmation')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
     
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection