 {!! Form::label('section', 'Sections', ['class' => 'required']) !!}
              <small class="text-primary">(Assign sections to user)</small>
              {!! Form::select('section[]', $selectAttr, 
                    null, 
                    ['class' => 'select2 form-control','data-placeholder' => 'Pick an option...', 'required' => true, 'multiple' => 'multiple']) 
                !!}
                @error('section')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  