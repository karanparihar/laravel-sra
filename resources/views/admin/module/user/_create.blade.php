@extends('admin.layouts.master')

@section('title', 'Add User')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
        
        {!! Form::open(['route' => 'users.store', 'files' => true]) !!}
        <div class="card-body">

          <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'required']) !!}
            {!! Form::text('name', old('name'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'required']) !!}
            {!! Form::text('email', old('email'), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
              {!! Form::label('sra', 'SRA', ['class' => 'required']) !!}
              {!! Form::select('sra', $selectSraType, 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick an option...', 'required' => true, 'id'=>'get-sections']) 
                !!}
                @error('sra')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  
          </div>

           <div class="form-group" id="user-sections" style="display: none;">
             
          </div>


          <div class="form-group">
              {!! Form::label('user role', 'Role', ['class' => 'required']) !!}
              {!! Form::select('role', $selectRole, 
                    null, 
                    ['class' => 'form-control','placeholder' => 'Pick an option...', 'required' => true]) 
                !!}
                @error('role')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  
          </div>


          <div class="form-group">
            {!! Form::label('password', 'Password', ['class' => 'required']) !!}
            {!! Form::text('password', null, array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('password')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('password-confirm', 'Confirm Password', ['class' => 'required']) !!}
            {!! Form::text('password_confirmation', null, array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
          </div>
        
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('sections.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection