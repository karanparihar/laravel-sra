@extends('admin.layouts.master')

@section('title', 'Edit User')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>
      
        {!! Form::open(['route' => ['users.update', $user->id], 'files' => true, 'method'=>'PUT']) !!}

         <div class="card-body">

          <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'required']) !!}
            {!! Form::text('name', old('name', $user->name), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('name')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('email', 'Email', ['class' => 'required']) !!}
            {!! Form::text('email', old('email', $user->email), array_merge(['class' => 'form-control'], ['required' => true] ) ) !!}
              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
              {!! Form::label('sra', 'SRA', ['class' => 'required']) !!}
              {!! Form::select('sra', $selectSraType, 
                    $user->sra, 
                    ['class' => 'form-control','placeholder' => 'Pick an option...', 'required' => true, 'id' => 'get-sections']) 
                !!}
                @error('sra')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  
          </div>

           <div class="form-group" id="user-sections">
              {!! Form::label('section', 'Sections', ['class' => 'required']) !!}
              <small class="text-primary">(Assign sections to user)</small>
              {!! Form::select('section[]', $selectAttr, 
                    explode(',',$user->sections), 
                    ['class' => 'select2 form-control','data-placeholder' => 'Pick an option...', 'required' => true, 'multiple' => 'multiple']) 
                !!}
                @error('section')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                  @enderror  
          </div>

          <div class="form-group">
              {!! Form::label('user role', 'Role', ['class' => 'required']) !!}
              {!! Form::select('role', $selectRole, 
                    $user->role, 
                    ['class' => 'form-control','placeholder' => 'Pick an option...', 'required' => true]) 
                !!}
                @error('role')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                @enderror  
          </div>
          
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
      {!! link_to_route('users.index',  'Cancel', $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->
@endsection