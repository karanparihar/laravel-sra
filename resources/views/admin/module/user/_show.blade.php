@extends('admin.layouts.master')

@section('title', $user->name)

@section('content')

<!-- Main content -->
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
               <div class="card-body box-profile">
                  <div class="text-center">
                     <img class="profile-user-img img-fluid img-circle"
                        src="{{ asset('assets/dist/img/user4-128x128.jpg') }}"
                        alt="User profile picture">
                  </div>
                  <h3 class="profile-username text-center">{{ $user->name }}</h3>
                  <p class="text-muted text-center">{{ $user->hasRole['name'] }} (<b>{{ $user->hasSra['name'] }}</b>)</p>
                  <ul class="list-group list-group-unbordered mb-3">
                    
                     <li class="list-group-item">
                        <b>Sections</b> <a class="float-right">{{ $sections->count() }}</a>
                     </li>
                     <li class="list-group-item">
                        <b>SRA</b> <a class="float-right">543</a>
                     </li>
                     
                  </ul>
                 {!! link_to_route('users.edit',  trans('app.actions.edit'), $parameters = [$user->id],  ['class' => 'btn btn-primary btn-block']) !!} 
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <!-- About Me Box -->
            <!-- <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">About Me</h3>
               </div>
               
               <div class="card-body">
                 <strong><i class="fas fa-book mr-1"></i> Education</strong>
               
                 <p class="text-muted">
                   B.S. in Computer Science from the University of Tennessee at Knoxville
                 </p>
               
                 <hr>
               
                 <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
               
                 <p class="text-muted">Malibu, California</p>
               
                 <hr>
               
                 <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>
               
                 <p class="text-muted">
                   <span class="tag tag-danger">UI Design</span>
                   <span class="tag tag-success">Coding</span>
                   <span class="tag tag-info">Javascript</span>
                   <span class="tag tag-warning">PHP</span>
                   <span class="tag tag-primary">Node.js</span>
                 </p>
               
                 <hr>
               
                 <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>
               
                 <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
               </div>
               
               </div> -->
            <!-- /.card -->
         </div>
         <!-- /.col -->
         <div class="col-md-9">
            <div class="card">
               <div class="card-header p-2">
                  <ul class="nav nav-pills">
                     <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Assigned Sections</a></li>
                  </ul>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div class="tab-content">
                     
                     <div class="active tab-pane" id="activity">
                        <div id="accordion">
                           @foreach($sections as $key => $section)
                           <div class="card card-{{ !empty($section->status == 1) ? 'success' : 'danger' }}">
                              <div class="card-header">
                                 <h4 class="card-title w-100">
                                    <a class="d-block w-100" data-toggle="collapse" href="#collapse{{ readNumber($section->id) }}">
                                    {{ $section->name }}
                                    </a>
                                 </h4>
                              </div>
                              <div id="collapse{{ readNumber($section->id) }}" class="collapse {{ !empty($key == 0) ? 'show' : '' }}" data-parent="#accordion">
                                 <div class="card-body">
                                  @if($section->formFields->count() > 0)
                                  
                                    @foreach($section->formFields as $data)
                                    
                                    <!------------ TEXT -------------->

                                    @if($data->field_type =='text')
                                    
                                       @include('admin.module.form-builder.fields._input', $data)
                                    
                                    <!------------ EMAIL -------------->

                                    @elseif($data->field_type =='email')
                                    
                                       @include('admin.module.form-builder.fields._input', $data)

                                    <!------------ URL -------------->
                                    
                                    @elseif($data->field_type == 'url')
                                      
                                      @include('admin.module.form-builder.fields._url', 
                                    $data)

                                    <!------------ NUMBER -------------->
                                    
                                    @elseif($data->field_type == 'number')
                                    
                                       @include('admin.module.form-builder.fields._number', 
                                    $data) 

                                    <!------------ TEXTAREA -------------->
                                    
                                    @elseif($data->field_type == 'textarea')
                                    
                                       @include('admin.module.form-builder.fields._textarea', $data) 

                                    <!------------ RADIO BUTTONS -------------->
                                    
                                    @elseif($data->field_type == 'radio-buttons')
                                    
                                       @include('admin.module.form-builder.fields._radio', $data) 
                                    
                                    <!------------ CHECKBOXES -------------->

                                    @elseif($data->field_type == 'checkboxes')
                                    
                                       @include('admin.module.form-builder.fields._checkbox', $data)

                                    <!------------ DROP DOWN -------------->
                                    
                                    @elseif($data->field_type == 'drop-down')
                                    
                                       @include('admin.module.form-builder.fields._select', 
                                    $data)

                                    <!------------ FILE -------------->
                                    
                                    @elseif($data->field_type == 'file')
                                    
                                       @include('admin.module.form-builder.fields._file', 
                                    $data)

                                    <!------------ TEL -------------->
                                    
                                    @elseif($data->field_type == 'tel')
                                    
                                       @include('admin.module.form-builder.fields._tel', 
                                    $data)

                                    <!------------ DATE -------------->
                                    
                                    @elseif($data->field_type == 'date')
                                    
                                       @include('admin.module.form-builder.fields._date', 
                                    $data)
                                    
                                    @else
                                    
                                    @endif
                                    
                                    @endforeach
                                 @else
                                    <div class="alert alert-danger alert-dismissible">
                                        
                                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                                        {{ trans('messages.alerts.form_field') }}
                                    </div>
                                 @endif

                                 </div>
                              </div>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection