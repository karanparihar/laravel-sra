@extends('admin.layouts.master')

@section('title', 'Edit Location')

@section('content')

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-10">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@yield('title')</h3>

        </div>

        {!! Form::open(['route' => ['location.update', $location->id], 'files' => false, 'method'=>'PUT']) !!}
        <div class="card-body">
         
         <div class="form-group">
            {!! Form::label('location', 'Location', ['class' => 'required']) !!}
            {!! Form::text('location', $location->location, array_merge(['class' => 'form-control', 'id' => 'location'], ['required' => true] ) ) !!}
              @error('location')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('address', 'Address', ['class' => 'required']) !!}
            {!! Form::text('address', $location->address, array_merge(['class' => 'form-control', 'id' => 'address'], ['required' => true] ) ) !!}
              @error('address')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('city', 'City', ['class' => 'required']) !!}
            {!! Form::text('city', $location->city, array_merge(['class' => 'form-control', 'id' => 'city'], ['required' => true] ) ) !!}
              @error('city')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('state', 'State', ['class' => 'required']) !!}
            {!! Form::text('state', $location->state, array_merge(['class' => 'form-control', 'id' => 'state'], ['required' => true] ) ) !!}
              @error('state')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('country', 'Country', ['class' => 'required']) !!}
            {!! Form::text('country', $location->country, array_merge(['class' => 'form-control', 'id' => 'country'], ['required' => true] ) ) !!}
              @error('country')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

          <div class="form-group">
            {!! Form::label('zipcode', 'Zip code', ['class' => 'required']) !!}
            {!! Form::text('zip_code', $location->zip_code, array_merge(['class' => 'form-control', 'id' => 'zip_code'], ['required' => true] ) ) !!}
              @error('zip_code')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>

        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
   
  </div>

  <div class="row">
    <div class="col-12">
      {!! Form::submit(trans('app.buttons.save'), ['class' => 'btn btn-success']) !!}
      {!! link_to_route('sections.index',  trans('app.buttons.cancel'), $parameters = [],  ['class' => 'btn btn-secondary']) !!} 
    </div>
  </div>
  {!! Form::close() !!}
</section>
<!-- /.content -->

@endsection