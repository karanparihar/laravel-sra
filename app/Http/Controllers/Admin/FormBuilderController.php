<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Fields\FormFieldStoreRequest;
use App\Models\Section;
use App\Models\FormField;
use App\Http\Traits\Common;
use DB;

class FormBuilderController extends Controller
{   
    use Common;
    
    private $model_name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->model_name = trans('app.model.form_field');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $formFields = FormField::orderBy('id', 'DESC')->get();
        
        return view('admin.module.form-builder._index', compact('formFields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormFieldStoreRequest $request)
    {  

        $orderDB = DB::table('form_fields')->where('section_id', $request->section_id)->max('ordering');
        
        $order = !empty($orderDB) ? $orderDB + 1 : 1;

        $saved = FormField::create( array_merge($request->all(), ['ordering' => $order ] ) );

        if( ! $saved)
            return response(['status' => trans('messages.failed')]);

        return response(['status' => trans('messages.created', ['model' => $this->model_name])]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FormField $manage_form)
    {   

        //return view('admin.module.form-builder._show', compact('manage_form'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FormField $manage_form)
    {   
        $sections = Section::where('status', 1)->get()->pluck('name', 'id');

        $attribute = $manage_form->field_type;

        return view('admin.module.form-builder._edit', compact('manage_form', 'attribute', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormField $manage_form)
    {   
     
        $_array = array(
            'section_id'        => $request->section_id ?? NULL,
            'field_type'        => $request->field_type ?? NULL,
            'required'          => $request->required ?? NULL,
            'name'              => $request->name ?? NULL,
            'values'            => $request->values ?? NULL,
            'placeholder'       => $request->placeholder ?? NULL,
            'min'               => $request->min ?? NULL,
            'max'               => $request->max ?? NULL,
            'multiple'          => $request->multiple ?? NULL,
            'include_blank'     => $request->include_blank ?? NULL,
            'label_first'       => $request->label_first ?? NULL,
            'limit'             => $request->limit ?? NULL,
            'filetypes'         => $request->filetypes ?? NULL,
            'class_attribute'   => $request->class_attribute ?? NULL,
            'id_attribute'      => $request->id_attribute ?? NULL,
        
        );
        
        if( FormField::where('id', $manage_form->id)->update($_array) )
            return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));

        return back()->with('error', trans('messages.failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()) {

            $delete = FormField::find($id)->delete();

            if( ! $delete)
                return response(['status' => trans('messages.failed')]);

            return response(['status' => trans('messages.deleted', ['model' => $this->model_name])]);
        }
    }

}
