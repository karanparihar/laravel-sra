<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SraMapping;
use App\Models\Sra;
use App\Models\FormField;

class SraMappingController extends Controller
{   
    private $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.sra_map');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       
        $fields = FormField::where(['section_id' => $request->section_id, 'status' => 1])->get();
    
        foreach ($fields as $innerKey => $innerValue) { 
            
            $tableArr[] = array(

                'meta_name'   =>  str_slug($innerValue->name),
                'meta_value'  =>  is_array($request->get(str_slug($innerValue->name)) ) ? implode(',', $request->get(str_slug($innerValue->name))) : $request->get(str_slug($innerValue->name)),
                'sra_id'      =>  $request->sra_id,
                'section_id'  =>  $request->section_id,
                'field_id'    =>  $innerValue->id,
             );         
        }
        
        $sra_mapped = SraMapping::insert($tableArr);

        if( ! $sra_mapped)
            return back()->with('error', trans('messages.failed'));

        return back()->with(['success' => trans('messages.created', ['model' => $this->model_name]), 'section_id' => $request->section_id]);
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sra = Sra::find($id);

        $tableArr = array(
              
              'is_previous'   => 1,
              'id_previous'   => $sra->id,
              'group'         => $sra->group,
              'provider'      => $sra->provider,
              'location'      => $sra->location,
              'year'          => date('Y')

        );

        $sraCreate = Sra::create($tableArr);

        $sraMap = SraMapping::where('sra_id', $id)->get();
        
        $tableArr1 = array();
        
        foreach ($sraMap as $innerKey => $innerValue) { 
            
            $tableArr1[] = array(

                'meta_name'   =>  $innerValue->meta_name,
                'meta_value'  =>  $innerValue->meta_value,
                'sra_id'      =>  $sraCreate->id,
                'section_id'  =>  $innerValue->section_id,
                'field_id'    =>  $innerValue->field_id,
             );         
        }

        $sra_mapped = SraMapping::insert($tableArr1);

        if( ! $sra_mapped)
            return back()->with('error', trans('messages.failed'));

        return back()->with(['success' => trans('messages.from_previous', ['model' => $this->model_name]), 'section_id' => $request->section_id ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        
        $fields = FormField::where(['section_id' => $request->section_id, 'status' => 1])->get();
       
        foreach ($fields as $innerKey => $innerValue) { 
            
            $tableArr  = array(

                'meta_name'   =>  str_slug($innerValue->name),
                'meta_value'  =>  is_array($request->get(str_slug($innerValue->name)) ) ? implode(',', $request->get(str_slug($innerValue->name))) : $request->get(str_slug($innerValue->name)),
                'sra_id'      =>  $request->sra_id,
                'section_id'  =>  $request->section_id,
                'field_id'    =>  $innerValue->id,
             );  

             $checkExist = SraMapping::where(['field_id' => $innerValue->id, 'section_id' => $request->section_id, 'sra_id' => $request->sra_id])->get();

             if($checkExist->count() > 0) {

                  $sra_mapped = SraMapping::where(['sra_id' => $request->sra_id, 'section_id' => $request->section_id, 'field_id' =>  $innerValue->id,])->update($tableArr);

             } else {

                  $sra_mapped = SraMapping::insert($tableArr);
            
             }

        }

        if( ! $sra_mapped)
            return back()->with('error', trans('messages.failed'));

        return back()->with(['success' => trans('messages.updated', ['model' => $this->model_name])]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
