<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SraDocuments;
use App\Http\Traits\Common;

class SraDocumentController extends Controller
{   
    use Common;

    private $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.sra_document');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
       
       if($request->hasFile('sra_document')) {
            
            $data = $this->uploadImage($request->file('sra_document'));
           
            if(!empty($data)) {

                $table_array = array();

                foreach ($data as $key => $value) {

                     $table_array[] = [
                         'sra_id'      => $request->sra_id,
                         'document'    => $value
                     ];
                }
               
                $document = SraDocuments::insert($table_array);

                if( ! $document)
                    return back()->with('error', trans('messages.failed'));

                return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
            }
      
            return back()->with('error', trans('messages.failed'));
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
