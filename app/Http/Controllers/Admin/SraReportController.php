<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sra;
use App\Models\SraMapping;
use App\Models\SraDocuments;
use PDF;
use ZipArchive;

class SraReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sraReport = Sra::all();
        
        $sraReport1 = array();

        foreach($sraReport as $key => $value) {

             $sraReport1[$value->year][$value->status][] = $value;
        }

        
        return view('admin.module.sra.report._report', compact('sraReport1'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sraCompleted = Sra::where('status', 1)->get();

        return view('admin.module.sra.report._completed', compact('sraCompleted'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sraReport = Sra::with(['sraMapped' => function ($q){
                        $q->orderBy('section_id', 'ASC');
                    }, 'sraDocuments'])->where(['id' => $id])->first();
        
        return view('admin.module.sra.report._index', compact('sraReport', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sraReport = Sra::with(['hasGroup', 'sraMapped' => function ($q){
                        $q->orderBy('section_id', 'ASC');
                    }, 'sraDocuments'])->where(['id' => $id])->first();

        $pr = [];
        
        foreach($sraReport->hasGroup['hasProviders'] as $innerValue) {
            
            $pr[] = $innerValue->name ;

        }

        $data = ['sraReport' => $sraReport, 'providers' => implode(', ', $pr), 'group' => $sraReport->hasGroup['name'] ];

        $pdf = PDF::loadView('admin.module.sra.report._pdf', $data);

        return $pdf->download($sraReport->hasGroup['name'].'-'.implode(', ', $pr).'.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
