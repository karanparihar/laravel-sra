<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Section;
use App\Models\Role;
use App\Models\SraType;
use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Traits\Common;

class UsersController extends Controller
{   

    private $model_name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['hasRole', 'hasSra'])->where('id', '!=', 1)->orderBy('id', 'DESC')->get();
        
        return view('admin.module.user._index', compact('users') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        if($request->ajax()) {
               
            $selectAttr   = Section::sectionAttributesBySra($request->sra);

            $return[] = view('admin.module.user._ajax-section')->with(['selectAttr' => $selectAttr])->render();

            return response()->json(['message' => $return ]);
        }

        
        $selectRole     = Role::roleAttributes();
        $selectSraType  = SraType::sraTypeAttributes();
    
        return view('admin.module.user._create', compact('selectRole', 'selectSraType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $userArr = [ 
                'name'     => $request->name, 
                'email'    => $request->email,
                'password' => Hash::make($request->password), 
                'sections' => implode(',', $request->section),
                'role'     => $request->role,
                'sra'      => $request->sra
        ];

        $user = User::create($userArr);
        if( ! $user)
            return back()->with('error', trans('messages.failed'));

        return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {   

        $sections = Section::with(['formFields' => function ($q){
                        $q->orderBy('ordering', 'ASC');
                    }])->whereIn('id', explode(',', $user->sections))->orderBy('ordering', 'ASC')->get();
       
        return view('admin.module.user._show', compact('user', 'sections'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {   
        $selectAttr    = Section::sectionAttributesBySra($user->sra);
        $selectRole    = Role::roleAttributes();
        $selectSraType = SraType::sraTypeAttributes();
        
        return view('admin.module.user._edit', compact('user', 'selectAttr', 'selectRole', 'selectSraType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
          $userArr = [ 
                'name'     => $request->name, 
                'email'    => $request->email,
                'sections' => implode(',', $request->section),
                'role'     => $request->role,
                'sra'      => $request->sra
        ];
        $user = User::where('id', $user->id)->update($userArr);
        if( ! $user)
            return back()->with('error', trans('messages.failed'));

        return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
