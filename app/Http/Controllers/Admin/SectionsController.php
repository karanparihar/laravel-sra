<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\FormField;
use App\Models\SectionOrder;
use App\Models\SraType;
use App\Http\Requests\Section\SectionStoreRequest;
use App\Http\Requests\Section\SectionUpdateRequest;
use DB;

class SectionsController extends Controller
{   
    private $model_name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->model_name = trans('app.model.section');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {   
        $sections = Section::with(['hasSra'])->orderBy('id', 'DESC')->get();
        
        return view('admin.module.section._index', compact('sections') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $SraTypeAttr = SraType::sraTypeAttributes();

        return view('admin.module.section._create', compact('SraTypeAttr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SectionStoreRequest $request)
    {   

        $orderDB = DB::table('sections')->max('ordering');
        
        $order = !empty($orderDB) ? $orderDB + 1 : 1;

        $section = Section::create( array_merge($request->all(), ['ordering' => $order ] ));

        if( ! $section)
            return back()->with('error', trans('messages.failed'));

        return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {   
        $form_fields = FormField::where(['section_id' => $section->id])->orderBy('ordering', 'ASC')->get();
        
        return view('admin.module.section._view', compact('section', 'form_fields'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {  
        $SraTypeAttr = SraType::sraTypeAttributes();

        return view('admin.module.section._edit', compact('section', 'SraTypeAttr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SectionUpdateRequest $request, Section $section)
    {   

        if( $section->update($request->all()) )
            return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));

        return back()->with('error', trans('messages.failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
