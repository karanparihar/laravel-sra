<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\SectionOrder;
use App\Models\SraType;
use Response;
use DB;

class SectionSortingController extends Controller
{  
    public function __construct()
    {
        $this->middleware(['auth']);
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $activeSections  = Section::orderBy('ordering', 'ASC')->get();

        $sraType  = SraType::all();

        return view('admin.module.section-sorting._index', compact('activeSections', 'sraType'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->ajax()) {
              
              $loop = array_combine(range(1, count($request->order)), array_values($request->order));
            
              foreach($loop as $key => $value) {
        
                 $update = Section::where('id', $value['section_id'])->update(['ordering' => $key]);
              }
              
              if(! $update) 
                 return response(['status' => 'Fail']);

              return response(['status' => 'Done']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
