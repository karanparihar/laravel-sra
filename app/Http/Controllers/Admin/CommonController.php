<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use\App\Models\Sra;

class CommonController extends Controller
{
    
    public function index() {
        
        $sraCompleted = Sra::with(['hasGroup', 'hasLocation'])->orderBy('id', 'DESC')->where('status', 1)->get();
 
    	return view('admin.module.sra.report._completed', compact('sraCompleted'));
    }
}
