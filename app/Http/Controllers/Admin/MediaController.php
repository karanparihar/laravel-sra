<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SraType;
use App\Models\Media;
use App\Models\location;
use App\Http\Requests\Media\StoreMediaRequest;
use App\Http\Traits\Common;

class MediaController extends Controller
{    
    use Common;

    private $model_name;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.media');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $media = Media::with(['hasLocation', 'hasSraType'])->get()->groupBy('location');
        return view('admin.module.media._index', compact('media'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $SraTypeAttr  = SraType::sraTypeAttributes();

        $locations    = Location::selectAttributes();

        return view('admin.module.media._create', compact('SraTypeAttr', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMediaRequest $request)
    {
        if($request->hasFile('media')) {

            $data = $this->uploadImage($request->file('media'));
           
            if(!empty($data)) {

                $table_array = array();

                foreach ($data as $key => $value) {

                     $table_array[] = [
                         'location' => $request->location,
                         'sra'      => $request->sra,
                         'media'    => $value
                     ];
                }
               
                $media = Media::insert($table_array);

                if( ! $media)
                    return back()->with('error', trans('messages.failed'));

                return back()->with('success', trans('messages.created', ['model' => $this->model_name]));
            }

            return back()->with('error', trans('messages.failed'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $sraType = SraType::all();

        $media = Media::with(['hasLocation', 'hasSraType'])->where('location', $id)->get();
        
        $location  = Location::where('id', $id)->pluck('location');
        
        return view('admin.module.media._view', compact('media', 'sraType', 'location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
