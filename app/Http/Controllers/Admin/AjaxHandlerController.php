<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FormField;
use App\Models\Section;
use App\Models\Sra;
use Response;

class AjaxHandlerController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {

            $status = FormField::where('id', $request->id)->update(['status' => $request->status]);

           if( ! $status)
                return response(['status' => trans('messages.failed')]);

            return response(['status' => trans('messages.status', ['model' => 'Form Field'])]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createFormField(Request $request)
    {
        if($request->ajax()) {

            $sections = Section::where('status', 1)->get()->pluck('name', 'id');
            
            $headline = "Form-tag Generator:".$request->attribute;

            $return[] = view('admin.module.form-builder._ajax_form_field')->with(['attribute' => $request->attribute, 'sections' => $sections ])->render();
            
            return response()->json(['state' => $headline, 'message' => $return ]);
        }
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function showFormField(Request $request) {

        if($request->ajax()) {

            $formField = FormField::find($request->attribute);
            
            $return[] = view('admin.module.form-builder._show')->with(['data' =>  $formField ])->render();
            
            return response()->json(['state' => ucwords($formField->field_type. ' Field'), 'message' => $return ]);
        }
     }
     
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeSecionStatus(Request $request)
    {
        if($request->ajax()) {

            $status = Section::where('id', $request->id)->update(['status' => $request->status]);

           if( ! $status)
                return response(['status' => trans('messages.failed')]);

            return response(['status' => trans('messages.status', ['model' => 'Section'])]);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function changeSraStatus(Request $request) {

        if($request->ajax()) {
             
             $status = Sra::where(['id' => $request->id ])->update(['status' => 1]);

             if( ! $status)
                return response(['status' => trans('messages.failed')]);

             return response(['status' => trans('messages.status', ['model' => 'Sra'])]);

        }
    }

}
