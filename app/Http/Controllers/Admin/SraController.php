<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Sra\StoreSraRequest;
use App\Http\Requests\Sra\UpdateSraRequest;
use App\Models\Sra;
use App\Models\Group;
use App\Models\Section;
use App\Models\Provider;
use App\Models\Location;
use App\Models\SraMapping;
use Session;
use DB;
use Auth;

class SraController extends Controller
{   
    private $model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->model_name = trans('app.model.sra');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $sra = Sra::with(['hasGroup', 'hasLocation'])->orderBy('id', 'DESC')->get();
        
        return view('admin.module.sra._index', compact('sra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        if($request->ajax()) {
              
              $provider = Provider::selectAttr($request->group);
              
              $return[] = view('admin.module.sra._ajax-provider')->with(['provider' => $provider])->render();

            return response()->json(['message' => $return ]);
        }

        $groups    = Group::selectAttr();

        $locations = Location::selectAttributes(); 

        return view('admin.module.sra._create', compact('groups', 'locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSraRequest $request)
    {   
        
        if($request->previous_year == 1) {
            
           $sra = Sra::with(['hasGroup', 'hasLocation'])->where('group', $request->group)->where('year', '<' , $request->year)->where('location', $request->location)->whereIn('provider' , [implode(',', $request->provider)] )->get();
           
           if($sra->count() > 0) {

                $groups    = Group::selectAttr();

                $locations = Location::selectAttributes(); 
                
                return view('admin.module.sra._create', compact('sra', 'groups', 'locations'));

           } else {

               return back()->with('error', ' No result found');

           } 

        } else  {
            
            $sra = Sra::create( array_merge($request->all(), ['provider' => implode(',', $request->provider) ] ));

            if( ! $sra)
                return back()->with('error', trans('messages.failed'));

            return back()->with(['success' => trans('messages.created', ['model' => $this->model_name]), 'data'    => $sra ]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        
        $sra = Sra::find($id);
   
        if(empty($sra)) {

            return redirect( route('sra.index'));
        }
       
        $sections = Auth::user()->sections;
        
        $assignedSections = explode(',', $sections);

        $sections = Section::with(['formFields' => function ($q){
                        $q->orderBy('ordering', 'ASC')->where('status', 1);
                    }, ])->orderBy('ordering', 'ASC')->get();
        
        $sraMapCount = SraMapping::where('sra_id', $id)->count();

        return view('admin.module.sra._show', compact('sections', 'id', 'assignedSections', 'sraMapCount', 'sra'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sra $sra)
    {  
        
        $groups    = Group::selectAttr();

        $locations = Location::selectAttributes(); 

        $providers = Provider::where('group', $sra->group)->get()->pluck('name', 'id');

       return view('admin.module.sra._edit', compact('sra', 'groups', 'providers', 'locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSraRequest $request, Sra $sra)
    {
        if( $sra->update( array_merge($request->all(), ['provider' => implode(',', $request->provider) ] ) ) )
            return back()->with('success', trans('messages.updated', ['model' => $this->model_name]));

        return back()->with('error', trans('messages.failed'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
