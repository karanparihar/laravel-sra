<?php

namespace App\Http\Traits;
use App\Models\Student;

trait Common {
    
    public function dropDownOrder() {
         
        $ordering = [];

        for($i = 1; $i <= env('ORDERING_LEN'); $i++) {
            $ordering[] = $i;
        }

        return $ordering;
    }

    public function uploadImage($array) {
         
         $data = array();
    	 foreach($array as $image) {
            
                $filenameWithExt = $image->getClientOriginalName ();
                $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension       = $image->getClientOriginalExtension();
                $thumbnail       = md5($filename. '_'. time()).'.'.$extension;
                $image->storeAs('public/media/', $thumbnail);
                $data[]          = 'media/'.$thumbnail;

          }

          return $data;
    }

}