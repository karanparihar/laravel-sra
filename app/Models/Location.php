<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'location',
        'address',
        'city',
        'state',
        'country',
        'zip_code'
    ];

    /**
     * The attributes that are used to select.
     *
     * @var array
     */
    public static function selectAttributes() {
        
        $selectAttr = self::get()->pluck('location', 'id');

        return $selectAttr;
    }

}
