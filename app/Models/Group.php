<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    public static function selectAttr() {

        $selectAttr = Group::get()->pluck('name', 'id');

        return $selectAttr;
    }

    public function hasProviders() {

        return $this->hasMany('App\Models\Provider', 'group', 'id');
    }
}
