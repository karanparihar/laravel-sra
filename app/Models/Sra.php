<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Sra extends Model
{
    use HasFactory, SoftDeletes, Notifiable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_previous',
        'id_previous',
        'group',
        'provider',
        'location',
        'year',
        'deleted_at',
        'created_by',
        'updated_by'
  
    ];

    protected $dates = ['deleted_at'];
    
    
    public static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            $user = Auth::user();           
            $model->created_by = $user->id;
            $model->updated_by = $user->id;
        });
        
        static::updating(function($model)
        {
            $user = Auth::user();
            $model->updated_by = $user->id;
        });       
    }

    public function hasLocation() {
       
       return $this->belongsTo('App\Models\Location', 'location', 'id');
 
    }

    public function hasGroup() {

        return $this->belongsTo('App\Models\Group', 'group', 'id')->with('hasProviders');
    }

    public function sraMapped() {

        return $this->hasMany('App\Models\SraMapping', 'sra_id', 'id');
    }


    public function sraDocuments() {

        return $this->hasMany('App\Models\SraDocuments', 'sra_id', 'id');
    }

}
