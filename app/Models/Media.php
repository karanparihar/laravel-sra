<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'location',
        'sra',
        'media',
        'thumbnail'
    ];
    
    /**
     * return location name
     *
     * @var array
     */

    public function hasLocation() {

        return $this->belongsTo('App\Models\Location', 'location', 'id');
    }

     /**
     * return sra type name
     *
     * @var array
     */

    public function hasSraType() {

        return $this->belongsTo('App\Models\SraType', 'sra', 'id');
    }

    
}
