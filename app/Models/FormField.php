<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormField extends Model
{
     use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_id',
        'ordering',
        'field_type',
        'required',
        'name',
        'values',
        'placeholder',
        'class_attribute',
        'id_attribute',
        'max',
        'min',
        'multiple',
        'include_blank',
        'label_first',
        'limit',
        'filetypes',
        'status'
    ];

}
