<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'group',
        'description'
    ];

    public function hasGroup() {

        return $this->belongsTo('App\Models\Group', 'group', 'id');
    }

    public static function selectAttr($condition) {

        $selectAttr = \App\Models\Provider::where('group', $condition)->get()->pluck('name', 'id');

        return $selectAttr;
    }
}
