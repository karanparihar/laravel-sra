<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'ordering',
        'status',
        'sra'
    ];

    
     /**
     * The attributes that are used to select.
     *
     * @var array
     */
    public static function sectionAttributes() {
        
        $selectAttr = self::where('status', 1)->get()->pluck('name', 'id');

        return $selectAttr;
    }


     /**
     * The attributes that are used to select.
     *
     * @var array
     */

    public static function sectionAttributesBySra($sra) {
           
        $selectAttr = self::where(['status'=> 1, 'sra' => $sra])->get()->pluck('name', 'id');

        return $selectAttr;
    }

     /**
     * associations belongs to form fields
     *
     * @var array
     */
    
    public function formFields() {

        return $this->hasMany('App\Models\FormField', 'section_id', 'id');
    }

    /**
     * return the associated sra data of the user.
     *
     * @var array
     */
    public function hasSra() {

        return $this->belongsTo('App\Models\SraType', 'sra', 'id');
    }

   
    
   
}
